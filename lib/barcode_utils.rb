require 'barby/barcode/ean_13'
require 'barby/outputter/svg_outputter'

require 'rexml/document'
require 'rexml/parent'
require 'rexml/xpath'

module Barby
  class CustomSvgOutputter < SvgOutputter
    register :to_svg_with_data

    def initialize(barcode)
      super(barcode)
      self.height, self.margin, self.bmargin = 50, 2, 13
    end

    def font_size
      13
    end

    def to_svg_with_data
      svg = to_svg
      data = <<EOF
<text font-family="helvetica" x="#{full_width / 2}" y="#{full_height - 2}" font-size="#{font_size}" style="text-anchor: middle;">#{barcode.to_s}</text>
EOF

      doc = REXML::Document.new(svg)
      subdoc = REXML::Document.new(data)
      doc.root.insert_after("//g[@id = 'barcode']", subdoc.root_node)
      doc.to_s
    end
  end
end
