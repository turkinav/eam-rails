namespace :db do
  desc "Erase and fill database"
  task :populate => :environment do
    [User, WorkOrder, Task].each(&:delete_all)
    2.times do |n|
      User.create!(name:"User#{n + 1}", button_id:"100#{n + 1}")
    end
    user = User.find_by_button_id("1001")
    10.times do |n|
      user.tasks.create!(title: "Task#{n + 1}", status: 0)
    end
  end
end