module FilterByDatePeriod
  def period_utc
    @period_utc ||= period.first.to_time_in_current_zone.utc..(period.last + 1.day).to_time_in_current_zone.utc
  end

  def period
    @period ||= (date_to < date_from) ? date_from..date_from : date_from..date_to
  end

  def date_from
    @date_from ||= Date.parse(params[:date_from]) rescue @date_from = current_month_period.first
  end

  def date_to
    @date_to ||= Date.parse(params[:date_to]) rescue @date_to = current_month_period.last
  end

###
# Time period helpers
  def current_month_period
    current_date = Date.current
    Date.new(current_date.year, current_date.month)..Date.new(current_date.year, current_date.month, -1)
  end
end
