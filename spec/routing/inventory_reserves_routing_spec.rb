require "spec_helper"

describe InventoryReservesController do
  describe "routing" do

    it "routes to #index" do
      get("/inventory_reserves").should route_to("inventory_reserves#index")
    end

    it "routes to #new" do
      get("/inventory_reserves/new").should route_to("inventory_reserves#new")
    end

    it "routes to #show" do
      get("/inventory_reserves/1").should route_to("inventory_reserves#show", :id => "1")
    end

    it "routes to #edit" do
      get("/inventory_reserves/1/edit").should route_to("inventory_reserves#edit", :id => "1")
    end

    it "routes to #create" do
      post("/inventory_reserves").should route_to("inventory_reserves#create")
    end

    it "routes to #update" do
      put("/inventory_reserves/1").should route_to("inventory_reserves#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/inventory_reserves/1").should route_to("inventory_reserves#destroy", :id => "1")
    end

  end
end
