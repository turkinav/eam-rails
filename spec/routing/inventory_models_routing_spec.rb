require "spec_helper"

describe InventoryModelsController do
  describe "routing" do

    it "routes to #index" do
      get("/inventory_models").should route_to("inventory_models#index")
    end

    it "routes to #new" do
      get("/inventory_models/new").should route_to("inventory_models#new")
    end

    it "routes to #show" do
      get("/inventory_models/1").should route_to("inventory_models#show", :id => "1")
    end

    it "routes to #edit" do
      get("/inventory_models/1/edit").should route_to("inventory_models#edit", :id => "1")
    end

    it "routes to #create" do
      post("/inventory_models").should route_to("inventory_models#create")
    end

    it "routes to #update" do
      put("/inventory_models/1").should route_to("inventory_models#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/inventory_models/1").should route_to("inventory_models#destroy", :id => "1")
    end

  end
end
