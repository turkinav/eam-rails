require "spec_helper"

describe InventoryArticlesController do
  describe "routing" do

    it "routes to #index" do
      get("/inventory_articles").should route_to("inventory_articles#index")
    end

    it "routes to #new" do
      get("/inventory_articles/new").should route_to("inventory_articles#new")
    end

    it "routes to #show" do
      get("/inventory_articles/1").should route_to("inventory_articles#show", :id => "1")
    end

    it "routes to #edit" do
      get("/inventory_articles/1/edit").should route_to("inventory_articles#edit", :id => "1")
    end

    it "routes to #create" do
      post("/inventory_articles").should route_to("inventory_articles#create")
    end

    it "routes to #update" do
      put("/inventory_articles/1").should route_to("inventory_articles#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/inventory_articles/1").should route_to("inventory_articles#destroy", :id => "1")
    end

  end
end
