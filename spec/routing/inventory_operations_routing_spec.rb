require "spec_helper"

describe InventoryOperationsController do
  describe "routing" do

    it "routes to #index" do
      get("/inventory_operations").should route_to("inventory_operations#index")
    end

    it "routes to #new" do
      get("/inventory_operations/new").should route_to("inventory_operations#new")
    end

    it "routes to #show" do
      get("/inventory_operations/1").should route_to("inventory_operations#show", :id => "1")
    end

    it "routes to #edit" do
      get("/inventory_operations/1/edit").should route_to("inventory_operations#edit", :id => "1")
    end

    it "routes to #create" do
      post("/inventory_operations").should route_to("inventory_operations#create")
    end

    it "routes to #update" do
      put("/inventory_operations/1").should route_to("inventory_operations#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/inventory_operations/1").should route_to("inventory_operations#destroy", :id => "1")
    end

  end
end
