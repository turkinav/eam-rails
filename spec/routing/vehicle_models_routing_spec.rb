require "spec_helper"

describe VehicleModelsController do
  describe "routing" do

    it "routes to #index" do
      get("/vehicle_models").should route_to("vehicle_models#index")
    end

    it "routes to #new" do
      get("/vehicle_models/new").should route_to("vehicle_models#new")
    end

    it "routes to #show" do
      get("/vehicle_models/1").should route_to("vehicle_models#show", :id => "1")
    end

    it "routes to #edit" do
      get("/vehicle_models/1/edit").should route_to("vehicle_models#edit", :id => "1")
    end

    it "routes to #create" do
      post("/vehicle_models").should route_to("vehicle_models#create")
    end

    it "routes to #update" do
      put("/vehicle_models/1").should route_to("vehicle_models#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/vehicle_models/1").should route_to("vehicle_models#destroy", :id => "1")
    end

  end
end
