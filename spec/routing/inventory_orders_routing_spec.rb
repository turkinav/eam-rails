require "spec_helper"

describe InventoryOrdersController do
  describe "routing" do

    it "routes to #search_index" do
      get("/inventory_orders").should route_to("inventory_orders#index")
    end

    it "routes to #new" do
      get("/inventory_orders/new").should route_to("inventory_orders#new")
    end

    it "routes to #show" do
      get("/inventory_orders/1").should route_to("inventory_orders#show", :id => "1")
    end

    it "routes to #edit" do
      get("/inventory_orders/1/edit").should route_to("inventory_orders#edit", :id => "1")
    end

    it "routes to #create" do
      post("/inventory_orders").should route_to("inventory_orders#create")
    end

    it "routes to #update" do
      put("/inventory_orders/1").should route_to("inventory_orders#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/inventory_orders/1").should route_to("inventory_orders#destroy", :id => "1")
    end

  end
end
