module JsonPresenter
  def self.inventory inventory
    {
    'id' => inventory.id,
    'sku' => self.sku(inventory.sku),
    'amount' => inventory.value.to_s('F'),
    'stockroom' => self.stockroom(inventory.stockroom)
    }
  end

  def self.inventory_amount inventory
    {
    'sku' => self.sku(inventory.sku),
    'amount' => inventory.value.to_s('F')
    }
  end


  def self.sku sku
    {
    'id' => sku.id,
    'model' => self.inventory_model(sku.model),
    'uom' => self.measuring_unit(sku.uom),
    'barcode' => self.barcode(sku.barcode)
    }
  end

  def self.stockroom stockroom
    {}
  end

  def self.inventory_model model
    {
    'id' => model.id,
    'title' => model.title,
    'description' => model.description
    }
  end

  def self.measuring_unit uom
    {
    'id' => uom.id,
    'title' => uom.title,
    'description' => uom.description
    }
  end

  def self.barcode barcode
    {
    'type' => 'ean13',
    'value' => barcode
    }
  end

end