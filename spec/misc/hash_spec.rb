require 'spec_helper'

describe 'hash' do
  describe 'merge' do
    let(:h1) { {id: 1, title: 'Test'} }
    let(:h2) { {id: 2} }
    before { @h3 = h1.merge(h2) }

    it { @h3[:id].should == 2 }
    it { @h3[:title].should == 'Test' }
  end
end