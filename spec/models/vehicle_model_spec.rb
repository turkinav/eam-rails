require 'spec_helper'

describe VehicleModel do
  it { should respond_to(:title) }
  it { should respond_to(:description) }
  it { should respond_to(:section_count) }
end
