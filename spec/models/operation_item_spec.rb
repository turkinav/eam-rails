require 'spec_helper'

describe OperationItem do
  it { should respond_to :operation }
  it { should respond_to :operation_id }
  it { should respond_to :sku }
  it { should respond_to :sku_id }
  it { should respond_to :amount }
end
