require 'spec_helper'

describe InventoryOperation do
  it { should respond_to :type }
  it { should respond_to :registration_time }
  it { should respond_to :posted }
  it { should respond_to :warehouse }
  it { should respond_to :warehouse_id }
  it { should respond_to :items }

  describe 'items' do
    subject { InventoryOperation.new.items }
    it { should respond_to(:by_sku_id) }
    it { should respond_to(:for) }
    it { should respond_to(:live) }
    it { should respond_to(:add) }
    it { should respond_to(:remove) }
  end

  describe ReceiveInventory do
    let(:receive_inventory) { FactoryGirl.create(:receive_inventory) }
    let(:sku) { FactoryGirl.create(:sku) }

    subject { receive_inventory }


    context 'when item with valid amount is added' do

      let(:valid_amount) { 123 }

      before do
        receive_inventory.items.add(sku, valid_amount)
      end

      its('items.size') { should == 1 }
      it { should be_valid }

      context 'when saved' do
        before do
          receive_inventory.save!
          receive_inventory.items.reload
        end
        its('items.size') { should == 1 }
        it { receive_inventory.items.for(sku).sku_id.should == sku.id }
        it { receive_inventory.items.for(sku).amount.should == valid_amount }

        context 'when amount is reduced to 0' do
          before do
            receive_inventory.items.remove(sku, valid_amount)
            receive_inventory.save
          end
          its('items.size') { should == 0 }
        end
      end
    end

    context 'when item with amount 0 is added' do
      let(:amount_zero) { 0 }

      before do
        receive_inventory.items.add(sku, amount_zero)
      end

      its('items.size') { should == 0 }
      it { should be_valid }
    end


    context 'when item with negative amount is added' do
      let(:negative_amount) { -1 }
      it { expect { receive_inventory.items.add(sku, negative_amount) }.to raise_error(ArgumentError) }
    end
  end

  describe 'accounting' do
    let(:warehouse) { FactoryGirl.create(:warehouse) }
    let(:sku) { FactoryGirl.create(:sku) }
    let(:operations) { [FactoryGirl.create(:receive_inventory), FactoryGirl.create(:write_off_inventory)] }

    before do
      operations.each do |op|
        op.warehouse = warehouse
        op.save!
      end
    end

    context 'when received' do
      before do
        operations[0].items.add(sku, 2)
        operations[0].apply
      end
      subject { Inventory.find_by_warehouse_id_and_sku_id(warehouse.id, sku.id) }
      its(:amount) { should == operations[0].items[0].amount }

      context 'when consumed' do
        before do
          operations[1].items.add(sku, 1)
          operations[1].apply
        end
        subject { Inventory.find_by_warehouse_id_and_sku_id(warehouse.id, sku.id) }
        its(:amount) { should == 1 }


        context 'when initial amount corrected' do
          let(:correction) { FactoryGirl.create(:receive_inventory) }
          before do
            correction.warehouse = warehouse
            correction.registration_time = operations[0].registration_time - 1.minute
            correction.items.add(sku, 1)
            correction.save!
            correction.apply
          end
          subject { Inventory.find_by_warehouse_id_and_sku_id(warehouse.id, sku.id) }
          its(:amount) { should == 2 }
        end
      end
    end
  end
end
