require 'spec_helper'

describe InventoryReserve do

  it { should respond_to :title }
  it { should respond_to :description }
  it {should respond_to :warehouse_id }
  it {should respond_to :warehouse }
  it { should respond_to :items }

  describe 'example reserve' do
    let(:reserve) { FactoryGirl.create(:inventory_reserve) }

    subject { reserve }

    context 'when valid reserve item added' do
      let(:sku) { FactoryGirl.create(:sku) }
      before do
        reserve.items.add(sku, 1)
      end

      subject { reserve }
      it { should be_valid }
      its('items.size') { should == 1 }

    #    context 'after reserve is saved' do
    #      before { reserve.save }
    #
    #      its('items') { should_not be_blank }
    #
    #      context 'when amount for the article is set to 0' do
    #        before do
    #          reserve.items_attributes = { '0' => {id: reserve.items.first.id, article_id: article.id, value: 0} }
    #        end
    #
    #        it { should be_valid }
    #        its('items') { should_not be_blank }
    #
    #        describe 'the reserve item' do
    #          let(:reserve_item) { give_to_user.items.first }
    #          subject { reserve_item }
    #          its('amount') { should == 0 }
    #        end
    #
    #        context 'when saved' do
    #          before { reserve.save }
    #          its('items') { should be_blank }
    #        end
    #      end
    #    end
    #  end
    #
    #  context 'when new reserve amount for the article is 0' do
    #    before do
    #      reserve.items_attributes = { '0' => {article_id: article.id, value: 0} }
    #    end
    #
    #    subject { give_to_user }
    #    its('items') { should_not be_blank }
    #    it { should be_valid }
    #
    #    describe 'the reserve item' do
    #      let(:reserve_item) { give_to_user.items.first }
    #      subject { reserve_item }
    #      its('amount') { should == 0 }
    #    end
    #
    #    context 'when saved' do
    #      before { reserve.save }
    #      its('items') { should be_blank }
    #    end
    end
  end
end
