require 'spec_helper'

describe Barcode do
  it { should respond_to :type }
  it { should respond_to :value }
  it { should respond_to :sku }

  context 'given some sku with barcode' do
    let(:sku) { FactoryGirl.create(:sku) }

    before do
      sku.build_barcode(type: 'Ean13', value: '1234567890128')
    end

    it { sku.barcode.should_not be_nil }

    it { sku.barcode.type.should == 'Ean13' }
    it { sku.barcode.class.should == Ean13 }


    context 'after sku saved' do
      before do
        sku.save!
        sku.reload
      end
      it { Barcode.count.should == 1 }
      it { sku.barcode.class.should == Ean13 }

      context 'when the sku is removed' do
        before { Sku.destroy sku }
        it { Barcode.count.should == 0 }
      end

      context 'given another sku' do

        let(:another_sku) { FactoryGirl.create(:sku) }
        it { another_sku.should be_valid }

        context 'when a barcode with the same value is built for it' do
          before do
            another_sku.build_barcode(type: 'Ean13', value: '1234567890128')
          end
          it { another_sku.should_not be_valid }
        end

        #NOTICE: Unobvious behavior
        context 'when it''s assigned the same barcode' do
          before do
            another_sku.barcode = sku.barcode
          end
          it { another_sku.should be_valid }
          it { sku.barcode(true).should be_nil }
          it { Barcode.first.sku.should == another_sku }
        end
      end
    end
  end
end
