require 'spec_helper'

describe Storage do
  it { should respond_to :order_num }
  it { should respond_to :warehouse }
  it { should respond_to :permissions }

  context "when permission granted to user" do
    let(:storage) { FactoryGirl.create(:storage) }
    let(:user) { FactoryGirl.create(:user) }

    before { storage.permissions.build(subject: user) }
    subject { storage }
    it { should be_valid }

    it "should prevent adding same user twice" do
      expect { permissions.build(subject: user) }.to raise_error
    end

    context "when permission granted to user group" do
      let(:storage) { FactoryGirl.create(:storage) }
      let(:group) { FactoryGirl.create(:group) }

      before { storage.permissions.build(subject: group) }
      subject { storage }
      it { should be_valid }

      it "should prevent adding same group twice" do
        expect { permissions.build(subject: group) }.to raise_error
      end
    end
  end
end
