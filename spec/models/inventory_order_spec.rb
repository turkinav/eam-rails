require 'spec_helper'

describe InventoryOrder do
  it { should respond_to :number }
  it { should respond_to :organization }
  it { should respond_to :contractor }
  it { should respond_to :items }

  let(:inventory) { FactoryGirl.create(:inventory) }
  let(:inventories) { [FactoryGirl.create(:inventory), FactoryGirl.create(:inventory1)]}

  let(:order) { FactoryGirl.create(:inventory_order) }
  subject { order }

  describe 'should ignore unsaved items marked for destruction in a nested update' do
    before do
      new_item = order.items.build(inventory: inventory, value: BigDecimal.new('10.0'))
      order.reload
      order.attributes = {
          :items_attributes => {
              '0' => {
                  'id' => new_item.id,
                  '_destroy' => '1'
              }
          }
      }
    end
    its('items.size') { should == 0 }
  end

  context 'when items added' do
    before {
      order.items.build(inventory: inventories[0], value: BigDecimal.new('1.0'))
      order.items.build(inventory: inventories[1], value: BigDecimal.new('2.0'))
      order.save
      order.reload
    }
    its('items.size') { should == 2 }

    describe 'should mark first item for destruction in a nested update' do
      before do
        order.attributes = {
            :items_attributes => {
                '0' => {
                    'id' => order.items[0].id,
                    '_destroy' => '1'
                }
            }
        }
      end
      it { order.items[0].should be_marked_for_destruction }
      it { order.items[1].should_not be_marked_for_destruction }
    end

    describe 'should mark last item for destruction in a nested update' do
      before do
        order.attributes = {
            :items_attributes => {
                '1' => {
                    'id' => order.items[1].id,
                    '_destroy' => '1'
                }
            }
        }
      end
      it { order.items[0].should_not be_marked_for_destruction }
      it { order.items[1].should be_marked_for_destruction }
    end

  end



end
