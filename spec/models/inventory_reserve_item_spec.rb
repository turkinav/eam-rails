require 'spec_helper'

describe InventoryReserveItem do
  it { should respond_to(:reserve) }
  it { should respond_to(:reserve_id) }
  it { should respond_to(:sku) }
  it { should respond_to(:sku_id) }
  it { should respond_to(:amount) }
end
