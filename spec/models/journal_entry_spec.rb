require 'spec_helper'

describe JournalEntry do
  it { should respond_to(:source_account) }
  it { should respond_to(:source_account_type) }
  it { should respond_to(:source_account_id) }
  it { should respond_to(:dest_account) }
  it { should respond_to(:dest_account_type) }
  it { should respond_to(:dest_account_id) }
  it { should respond_to(:sku) }
  it { should respond_to(:sku_id) }
  it { should respond_to(:amount) }
  it { should respond_to(:valid_time) }
end
