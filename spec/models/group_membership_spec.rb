require 'spec_helper'

describe GroupMembership do
  it { should respond_to :user }
  it { should respond_to :group }
end
