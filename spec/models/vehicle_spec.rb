require 'spec_helper'

describe Vehicle do
  it { should respond_to(:model) }
  it { should respond_to(:model_id) }
  it { should respond_to(:number) }
  it { should respond_to(:section_index) }
  it { should respond_to :credit_entries }
  it { should respond_to :debit_entries }
end
