require 'spec_helper'

describe InventoryModel do
  it { should respond_to :title }
  it { should respond_to :description }
end
