require 'spec_helper'

describe MeasuringUnit do
  it { should respond_to :title }
  it { should respond_to :description }
end
