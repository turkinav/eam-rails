require 'spec_helper'

describe Group do
  it { should respond_to :title  }
  it { should respond_to :description  }
  it { should respond_to :users  }
end
