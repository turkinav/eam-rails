require 'spec_helper'

describe Warehouse do
  it { should respond_to :title  }
  it { should respond_to :description  }
  it { should respond_to :inventories }
  it { should respond_to :credit_entries }
  it { should respond_to :debit_entries }
end
