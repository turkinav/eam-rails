require 'spec_helper'

describe Inventory do
  it { should respond_to :warehouse }
  it { should respond_to :sku }
  it { should respond_to :amount }
  it { should respond_to :holding_amount }
  it { should respond_to :holdings_total_amount }
  it { should respond_to :available_amount }

  context 'when allocated' do
    let(:inventory) do
      FactoryGirl.create(:inventory, amount: 10)
    end

    subject { inventory }

    context 'is taken' do
      let(:user) { FactoryGirl.create(:user) }

      before do
        inventory.give_to_user(user, 1)
      end

      it { inventory.holding_amount(user).should == 1 }
      it { inventory.holdings_total_amount.should == 1 }
      it { inventory.available_amount.should == 9 }
      it { inventory.amount.should == 10 }

      context 'when saved' do
        before { inventory.save! }

        it { inventory.holding_amount(user).should == 1 }
        it { inventory.holdings_total_amount.should == 1 }
        it { inventory.available_amount.should == 9 }
        it { inventory.amount.should == 10 }
      end

      context 'is returned' do
        before do
          inventory.take_from_user(user, 1)
        end

        it { inventory.holding_amount(user).should == 0 }
        it { inventory.holdings_total_amount.should == 0 }
        it { inventory.available_amount.should == 10 }
        it { inventory.amount.should == 10 }
      end

      it 'prevents taking more than is available' do
        lambda { inventory.give_to_user(user, inventory.available_amount + 1) }.should raise_error ArgumentError
      end

      it 'prevents returning more than is taken' do
        lambda { inventory.take_from_user(user, inventory.holding_amount(user) + 1) }.should raise_error ArgumentError
      end
    end
  end
end