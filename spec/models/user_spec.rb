# encoding: utf-8
require 'spec_helper'

describe User do
  it { should respond_to :uuid }
  it { should respond_to :login }
  it { should respond_to :password }
  it { should respond_to :name }
  it { should respond_to :groups }

  describe 'new user' do
    let(:user) { FactoryGirl.build(:user) }
    subject { user }
    its(:uuid) { should_not be_nil }
  end
end
