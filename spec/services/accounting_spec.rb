require 'spec_helper'

describe Accounting do
  describe Accounting::ReceiveInventoryHandler do
    let(:operation) { FactoryGirl.create(:receive_inventory) }
    let(:handler) { Accounting::ReceiveInventoryHandler.new(operation) }

    let(:sku) { FactoryGirl.create(:sku) }
    let(:sku_amount) { rand(1..10) }

    before { operation.items.add(sku, sku_amount) }

    subject { handler }
    it { should respond_to(:execute) }
    it { operation.should be_valid }

    context 'when handler executed' do

      let(:inventories) { operation.items.map { |item| Inventory.new(sku: item.sku, warehouse: operation.warehouse) } }

      before { handler.execute(inventories) }

      it { handler.entries.length.should == operation.items.length }

      it 'handler entries should match operation items' do
        handler.entries.each do |entry|
          entry.should be_valid
          entry.valid_time.should == operation.registration_time
          entry.dest_account.should == operation.warehouse
          operation.items.for(entry.sku).should_not be_nil
          operation.items.for(entry.sku).amount.should == entry.amount
        end
      end
    end
  end

end




