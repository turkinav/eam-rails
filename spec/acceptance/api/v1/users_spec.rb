# encoding: utf-8

require 'spec_helper'
require 'rspec_api_documentation/dsl'

resource "Users" do

  #TODO: extract to module (?)

  header "Accept", "application/json"
  header "Content-Type", "application/json"

  head "/api/v1/users" do
    example_request "HEADERS" do
      response_headers["Content-Type"].should == "application/json; charset=utf-8"
    end
  end

  let(:user1) { FactoryGirl.create(:user1) }
  let(:user2) { FactoryGirl.create(:user2) }
  let(:users) { [user1, user2] }

  before { users.each { |user| user.save } }

  get "/api/v1/users" do
    example_request "Index" do
      JSON.parse(response_body).should == User.all.map { |user| user_json(user) }
      status.should == 200
    end
  end

  get "/api/v1/users/:id" do

    parameter :id, "User ID"
    let(:id) { user1.id }

    example_request "Show" do
      JSON.parse(response_body).should == user_json(user1)
      status.should == 200
    end
  end

  get "/api/v1/users?button_id=:button_id" do

    parameter :button_id, "User button ID"
    let(:button_id) { user2.button_id }

    example_request "Find by button number" do
      JSON.parse(response_body).should == [ user_json(user2) ]
      status.should == 200
    end
  end

end

def user_json(user)
  {  "id" => user.id,  "login" => user.login,  "name" => user.name, "button_id" => user.button_id  }
end
