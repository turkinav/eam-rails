# encoding: utf-8

require 'spec_helper'
require 'rspec_api_documentation/dsl'


resource 'Inventories' do
  header "Accept", "application/json"
  header "Content-Type", "application/json"

  let(:warehouse) { FactoryGirl.create(:warehouse) }
  let(:user) { FactoryGirl.create(:user) }

  get '/api/inventories/available_amounts?warehouse_id=:warehouse_id' do
    parameter :warehouse_id, ''
    let(:warehouse_id) { warehouse.id }

    example_request 'Получение списка свободных позиций' do
      JSON.parse(response_body).should == []
      status.should == 200
    end

    get '/api/inventories/available_amounts?warehouse_id=:warehouse_id&search_text=:search_text' do
      let(:search_text) { '' }

      example_request 'Поиск свободных позиций по наименованию или штрих-коду' do
        JSON.parse(response_body).should == []
        status.should == 200
      end
    end
  end

  get '/api/inventories/reserved_amounts?warehouse_id=:warehouse_id&user_id=:user_id' do
    parameter :warehouse_id, ''
    parameter :user_id, ''

    let(:warehouse_id) { warehouse.id }
    let(:user_id) { user.id }

    example_request 'Получение списка позиций, выданных пользователю' do
      JSON.parse(response_body).should == []
      status.should == 200
    end
  end


  post '/api/inventories/reserve?warehouse_id=:warehouse_id&user_id=:user_id' do
    parameter :warehouse_id, ''
    parameter :user_id, ''

    let(:warehouse_id) { warehouse.id }
    let(:user_id) { user.id }

    example_request 'Получение позиций со склада пользователем' do
      status.should == 200
    end
  end

  post '/api/inventories/release?warehouse_id=:warehouse_id&user_id=:user_id' do
    parameter :warehouse_id, ''
    parameter :user_id, ''


    let(:warehouse_id) { warehouse.id }
    let(:user_id) { user.id }

    example_request 'Возврат позиций на склад пользователем' do
      status.should == 200
    end
  end


end

def inventory_amount_json(inventory_amount)
  {}
end
