# encoding: utf-8

require 'spec_helper'
require 'rspec_api_documentation/dsl'

resource 'Warehouses' do
  header 'Accept', 'application/json'
  header 'Content-Type', 'application/json'

  let(:warehouse) { FactoryGirl.create(:warehouse) }
  let(:inventory1) { FactoryGirl.create(:inventory1)}
  let(:inventory2) { FactoryGirl.create(:inventory2)}
  let(:inventories) { [inventory1, inventory2] }

  before do
    warehouse.account.deposit(inventory1, 1)
    warehouse.account.deposit(inventory2, 2)
    warehouse.account.save
  end

  head '/api/warehouses' do
    example_request "HEADERS" do
      response_headers["Content-Type"].should == "application/json; charset=utf-8"
    end
  end

  get '/api/warehouses' do
    example_request "Index" do
      expected_json = [ { "id" => warehouse.id, "title" => warehouse.title } ]
      JSON.parse(response_body).should ==  expected_json
      status.should == 200
    end
  end

  get '/api/warehouses/:id' do

    parameter :id, 'ID склада'
    let(:id) { warehouse.id }

    example_request 'Show' do
      JSON.parse(response_body).should ==  warehouse_json(warehouse)
      status.should == 200
    end
  end

  get "/api/v1/organizations/:id/balance" do

    parameter :id, "Warehouse ID"

    let(:id) { warehouse.id }

    example_request "Show balance" do
      expected_json = [
        {
          "inventory" =>
            {
              "id" => inventory1.id,
              "title" => inventory1.title,
              "barcode" => inventory1.barcode
            },
          "amount" => 1.0
        },
        {
          "inventory" =>
            {
              "id" => inventory2.id,
              "title" => inventory2.title,
              "barcode" => inventory2.barcode
            },
          "amount" => 2.0
        }
      ]
      JSON.parse(response_body).should ==  expected_json
      status.should == 200
    end
  end

  get '/api/v1/organizations/:id/balance?inventory_id=:inventory_id' do

    parameter :id, "Warehouse ID"
    parameter :inventory_id, "Inventory ID"

    let(:id) { warehouse.id }
    let(:inventory_id) { inventory2.id }

    example_request "Show given item balance" do
      expected_json = [
          {
              "inventory" =>
                  {
                      "id" => inventory2.id,
                      "title" => inventory2.title,
                      "barcode" => inventory2.barcode
                  },
              "amount" => 2.0
          }
      ]
      JSON.parse(response_body).should ==  expected_json
      status.should == 200
    end
  end
end

def warehouse_json(warehouse)
  {  'id' => warehouse.id, 'title' => warehouse.title  }
end
