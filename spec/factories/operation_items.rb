# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :operation_item do
    sku_id 1
    amount "9.99"
  end
end
