# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :vehicle_model do
    title "MyString"
    description "MyText"
  end
end
