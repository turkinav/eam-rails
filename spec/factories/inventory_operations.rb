FactoryGirl.define do

  factory :inventory_operation do

    number { SecureRandom.uuid }
    registration_time { Time.zone.now }
    association :warehouse

    after(:build) { |operation| build_items(operation) }

    factory :receive_inventory, class: ReceiveInventory do
    end

    factory :write_off_inventory, class: WriteOffInventory do
      # association :vehicle
      # association :worker
    end

    factory :take_inventory, class: TakeInventory do
      association :worker
    end

    factory :release_inventory, class: ReleaseInventory do
      association :worker
    end
  end
end

def build_items(operation)
end
