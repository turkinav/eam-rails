# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    login "User"
    password "123456"
    password_confirmation "123456"

    factory :user1 do
      login "User1"
      password "123456"
      password_confirmation "123456"
      button_id "1231283401"
    end

    factory :user2 do
      login "User2"
      password "123456"
      password_confirmation "123456"
      button_id "1208341908"
    end
  end
end
