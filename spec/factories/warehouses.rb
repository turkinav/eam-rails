# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :warehouse do
    title { "Warehouse #{SecureRandom.uuid}" }
    description 'Test warehouse'
  end
end
