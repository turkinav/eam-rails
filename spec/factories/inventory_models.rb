# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inventory_model do
    title { SecureRandom.uuid }
    description "Test inventory model"
  end
end
