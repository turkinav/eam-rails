# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :journal_entry do
    inventory_id 1
    type ""
    subject_type "MyString"
    subject_id 1
    amount "9.99"
    registration_time "2013-09-09 16:44:15"
  end
end
