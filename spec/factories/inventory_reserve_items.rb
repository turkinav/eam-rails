# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inventory_reserve_item do
    association :reserve, factory: :inventory_reserve
    association :sku
    amount { rand(1..10) }
    reorder_level { rand(1..10) }
    reorder_amount { rand(1..10) }
  end
end
