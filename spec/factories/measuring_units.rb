# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :measuring_unit do
    title { SecureRandom.uuid }
    description 'Test measuring unit'
  end
end
