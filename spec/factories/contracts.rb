# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contract do
    organization_id 1
    contractor_id 1
  end
end
