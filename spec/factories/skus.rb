# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :sku do
    association :model, factory: :inventory_model
    association :uom, factory: :measuring_unit

    trait :defective do
      defective true
    end
  end
end
