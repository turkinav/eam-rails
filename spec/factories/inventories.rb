# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inventory do
    association :sku
    association :warehouse
    amount { rand(1..10) }
  end
end
