# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inventory_reserve do
    title { SecureRandom.uuid }
    association :warehouse
  end
end
