# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :vehicle do
    association :model, factory: :vehicle_model
    number 1000
    section_index 1
  end
end
