# encoding: utf-8
require 'spec_helper'
require 'shared/json_presenter'

describe 'Stockrooms API' do
  let(:organization) { FactoryGirl.create(:organization) }
  let(:inventories) { FactoryGirl.build_list(:inventory, 5, stockroom: organization) }

  before {
    inventories.each {|inv| inv.save }
    p inventories.inspect
  }

  context 'GET api/v1/organizations/[:id]/available_inventories' do
    subject do
      get "api/v1/organizations/#{organization.id}/available_inventories"
      JSON.parse(response.body)
    end

    it 'должен вернуть JSON' do
      should == inventories.map {|inv| JsonPresenter.inventory_amount inv }
    end
  end

  context 'GET api/v1/organizations/[:id]/reserved_inventories' do
    subject do
      get "api/v1/organizations/#{organization.id}/reserved_inventories"
      JSON.parse(response.body)
    end

    it 'должен вернуть JSON' do
      should == []
    end
  end

end