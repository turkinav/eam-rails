# encoding: utf-8

require 'spec_helper'

def user_json(user)
  {
      "id" => user.id,
      "login" => user.login,
      "name" => user.name,
      "button_id" => user.button_id
  }
end

describe 'Users API' do

  let(:user1) { FactoryGirl.create(:user1) }
  let(:user2) { FactoryGirl.create(:user2) }

  context 'GET api/v1/users' do
    let(:base_url) { "api/v1/users" }

    before do
      user1.reload
      user2.reload
    end

    subject {
      get base_url
      JSON.parse(response.body)
    }

    it "должен вернуть JSON" do
      should == [ user_json(user1), user_json(user2) ]
    end

    context '?button_id=#{button_id}' do
      let(:search_tests) do
        {
          user1.button_id => [ user_json(user1) ],
          user2.button_id => [ user_json(user2) ],
          "notexist" => []
        }
      end

      it "должен вернуть JSON" do
        search_tests.each do |button_id, json|
          get (base_url + "?button_id=#{button_id}")
          JSON.parse(response.body).should == json
        end
      end
    end
  end
end
