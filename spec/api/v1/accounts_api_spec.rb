# encoding: utf-8

require 'spec_helper'

describe 'Accounts API' do
  let(:user) { FactoryGirl.create(:user) }
  let(:warehouse) { FactoryGirl.create(:warehouse) }
  let(:inventory1) { FactoryGirl.create(:inventory1)}
  let(:inventory2) { FactoryGirl.create(:inventory2)}

  context 'POST api/v1/users/#{user.id}/reserve_inventory.json' do

    let (:items) do
       [
        { inventory_id: inventory1.id, value: 1.0 },
        { inventory_id: inventory2.id, value: 2.0 }
       ]
    end

    before do
      warehouse.account.deposit(inventory1, 1.0)
      warehouse.account.deposit(inventory2, 2.0)
      warehouse.account.save

      post "api/v1/users/#{user.id}/reserve_inventory.json", items.to_json, "CONTENT_TYPE" => "text/json"

      user.reload
      warehouse.reload
    end

    it { response.should be_ok }
    it { JSON.parse(response.body).should == {} }
    it { warehouse.account.balance_for(inventory1).should == 0 }
    it { warehouse.account.balance_for(inventory2).should == 0 }
    it { user.account.balance_for(inventory1).should == 1.0 }
    it { user.account.balance_for(inventory2).should == 2.0 }
  end

  context 'POST api/v1/users/#{user.id}/release_inventory.json' do

    let (:items) do
      [
        { inventory_id: inventory1.id, value: 1.0 },
        { inventory_id: inventory2.id, value: 2.0 }
      ]
    end

    before do
      warehouse.reload

      user.account.deposit(inventory1, 1.0)
      user.account.deposit(inventory2, 2.0)
      user.account.save

      post "api/v1/users/#{user.id}/release_inventory.json", items.to_json, "CONTENT_TYPE" => "text/json"
      user.reload
      warehouse.reload
    end

    it { response.should be_ok }
    it { JSON.parse(response.body).should == {} }

    it { warehouse.account.balance_for(inventory1).should == 1.0 }
    it { warehouse.account.balance_for(inventory2).should == 2.0 }
    it { user.account.balance_for(inventory1).should == 0 }
    it { user.account.balance_for(inventory2).should == 0 }
  end

  context 'GET api/v1/users/#{user.id}/inventories.json' do

    before do
      user.account.deposit(inventory1, 1.0)
      user.account.deposit(inventory2, 2.0)
      user.account.save
    end

    subject do
      get "api/v1/users/#{user.id}/inventories.json"
      JSON.parse(response.body)
    end

    it 'должен вернуть JSON' do
      should == [
          {
          "inventory" =>
              {
              "id" => inventory1.id,
              "title" => inventory1.title,
              "barcode" => inventory1.barcode
              },
          "amount" => 1
          },
          {
          "inventory" =>
              {
              "id" => inventory2.id,
              "title" => inventory2.title,
              "barcode" => inventory2.barcode
              },
          "amount" => 2
          }
      ]
    end
  end
end