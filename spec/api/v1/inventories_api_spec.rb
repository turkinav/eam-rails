# encoding: utf-8

require 'spec_helper'
require 'shared/json_presenter'

describe 'Inventories API' do
  let(:organization) { FactoryGirl.create :organization  }
  let(:inventories) { FactoryGirl.build_list :inventory, 5, stockroom: organization }

  before { inventories.each { |inv| inv.save } }


  context 'GET api/v1/inventories' do
    let (:base_url) { 'api/v1/inventories' }

    subject do
      get base_url
      JSON.parse(response.body)
    end

    it 'должен вернуть JSON' do
      should == inventories.map { |inv| JsonPresenter.inventory_amount inv }
    end

    #context '?query=#{barcode}' do
    #
    #  subject {
    #    get base_url + "?query=#{inventory4.barcode}"
    #    JSON.parse(response.body)
    #  }
    #
    #  it "должен вернуть JSON" do
    #    should == [ inventory_json(inventory4) ]
    #  end
    #end
    #
    #context '?query=#{title_part}' do
    #  let(:title_part) { "inventory" }
    #
    #  subject {
    #    get base_url + "?query=#{title_part}"
    #    JSON.parse(response.body)
    #  }
    #
    #  it "должен вернуть JSON" do
    #    should == [inventory1, inventory2, inventory4].map { |item| inventory_json(item) }
    #  end
    #end
    #
    #context '?query=#{exact_title}' do
    #  let(:exact_title) { "inventory1" }
    #  subject {
    #    get base_url + "?query=#{exact_title}"
    #    JSON.parse(response.body)
    #  }
    #  it "должен вернуть JSON" do
    #    should == [inventory_json(inventory1)]
    #  end
    #end
    #
    #context '?query=#{no_match}' do
    #  subject {
    #    get base_url + "?query=no_match"
    #    JSON.parse(response.body)
    #  }
    #  it { should be_empty }
    #end

  end
end
