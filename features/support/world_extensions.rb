# encoding: utf-8

require 'rack/test'
require 'authlogic/test_case'

module Utils
  def replace_keys(hash, mappings)
    Hash[hash.map {|key, value| [mappings[key], value] }]
  end
end

module KnowsTheDomain

  def create_inventory(options = {})
    Inventory.create!(options)
  end

  def create_user(options = {})
    User.create!(options)
  end

  def user(user_name)
    User.find_by_name(user_name)
  end

  def user_cart(user)
    user.account.cart
  end

  def my_asset
    @my_asset ||= Inventory.new(id: 1, title: "Test inventory")
  end

  def my_user
    @my_user ||= User.new(id: 1, name: "Test user")
  end

  def my_user=(user)
    @my_user = user
  end

  def my_user_acc
    my_user.account
  end

  def transfer(asset, amount, from, to)
    bookkeeper.transfer(asset, amount, from, to)
  end

  def bookkeeper
    @booking_service ||= BookingService.new
  end

  def inventory(title)
    Inventory.find_by_title(title)
  end

  #TODO: create_inventory()
  #TODO: raise Error if entity not found
  def inventory!(title)
    result = Inventory.find_or_create_by_title(title)
    result.save
    result
  end

  def create_warehouse(a_title)
    Warehouse.create!(title: a_title)
  end

  def warehouse!(a_title)
    Warehouse.find_by_title!(a_title)
  end

  def account!(a_title)
    Account.find_by_title!(a_title)
  end

  def model!(title)
    title.match(/(\w+)(\d+)/) do |m|
      $1.camelcase.constantize.find_by_title(m.to_s)
    end
  end
end

World(Rack::Test::Methods, KnowsTheDomain, Authlogic::TestCase, Utils)

Before do
  activate_authlogic
end
