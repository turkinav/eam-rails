CAPTURE_MODEL = Transform /^the (.+)$/  do |model_name|
  self.model_name = model_name.capitalize
end