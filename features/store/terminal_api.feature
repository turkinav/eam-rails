# language: ru

Функция: API для терминала склада
  Предыстория:
    Допустим есть склад
    И есть пользователь
    И в систему внесены следующие позиции:
      | наименование |
      | inventory1   |
      | inventory2   |
      | inventory3   |


  Сценарий: Склад по умолчанию
    Пусть склад по умолчанию - "warehouse1"
    Если я отправляю системе GET запрос "/api/warehouses/default" в формате JSON
    То система должна вернуть JSON:
      """
      { "id": 1, "title": "warehouse1" }
      """

  Сценарий: Смотреть остатки на складе
    Пусть на складе "warehouse1" есть следующие остатки:
      | наименование  | количество  |
      | inventory1    | 1           |
      | inventory2    | 2           |
      | inventory3    | 3           |
    Если я отправляю системе GET запрос "/api/warehouses/{warehouse1}/account" в формате JSON
    То система должна вернуть JSON:
    """
      {
        "id": 1,
        "title": "warehouse1",
        "cart":
        {
          "items":
          [
            { "inventory_id": 1, "title": "inventory1", "amount": "1.0" },
            { "inventory_id": 2, "title": "inventory2", "amount": "2.0" },
            { "inventory_id": 3, "title": "inventory3", "amount": "3.0" }
          ]
        }
      }
      """






