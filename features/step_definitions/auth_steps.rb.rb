# encoding: utf-8

When /^зарегистрирован следующий пользователь:$/ do |table|
  table.hashes.each do |row|
    User.create do |user|
      user.name = user.login = row["имя"]
      user.password = user.password_confirmation = row["пароль"]
    end
  end
end
И /^я не выполнил вход в систему$/ do
  current_session = UserSession.find
  current_session.destroy if current_session
end
Если /^я ввожу имя "(.+)" и пароль "(.+)"$/ do |login, password|
  user_session = UserSession.new(login: login, password: password)
  user_session.save.should
end
Тогда /^текущий пользователь должен быть "(.+)"$/ do |login|
  current_session = UserSession.find
  current_session.user.login.should == login
end
When /^я выполнил вход в систему$/ do
  current_session = UserSession.find
  UserSession.create(User.first) unless current_session
end
When /^я выхожу из системы$/ do
  UserSession.find.destroy
end
Then /^текущий сеанс должен быть закрыт$/ do
  UserSession.find.should_not
end
When /^я не должен войти$/ do
  step "текущий сеанс должен быть закрыт"
end
When /^я пытаюсь получить доступ к системе$/ do
  pending
end
Then /^я должен выполнить вход в систему$/ do
  pending
end
When /^я пытаюсь получить доступ к ресурсу (.+)$/ do |resource|
  visit send("#{resource}_path")
end
Then /^я должен перейти к ресурсу (.+)$/ do |resource|
  URI.parse(current_url).path.should == send("#{resource}_path")
end
When /^должен перейти на страницу аутентификации$/ do
  URI.parse(current_url).path.should == login_path
end
When /^я ввожу следующие учётные данные:$/ do |table|
  within("#new_user_session") do
    fill_in "user_session_login", with: table.hashes[0]["имя"]
    fill_in "user_session_password", with: table.hashes[0]["пароль"]
    click_on "Войти"
  end
end
