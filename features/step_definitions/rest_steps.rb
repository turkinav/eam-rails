#encoding: utf-8

Если /^я отправляю системе GET запрос "([^\"]*)"$/ do |path|
  method = :get
  send(method, path)
end

То /^система должна вернуть JSON:$/ do |json|
  JSON.parse(last_response.body).should == JSON.parse(json)
end

When /^я отправляю системе (POST|GET) запрос "(.+)" в формате (JSON|XML)$/ do |method, path, format, *body|
  method  = method.downcase.to_sym
  format  = format.downcase

  path = path.gsub(/\{(\w+)\}/) { self.model!($1).to_param }

  header('Accept', "application/#{format}")
  header('Content-Type', "application/#{format}")
  send(method, path, body.first)
end
When /^код ответа должен быть (\d+)$/ do |response_code|
  last_response.status.should == response_code.to_i
end




