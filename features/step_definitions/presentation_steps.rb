# encoding: utf-8

То(/^система должна показать следующие поля:$/) do |table|
  # table is a Cucumber::Ast::Table
  table.raw.each do |row|
    page.should have_field row[0]
  end
end

When(/^я ввожу следующие данные:$/) do |table|
  table.raw.each do |row|
    input_name = row[0].strip + ':'
    value = row[1].strip
    page.fill_in(input_name, with: value) if page.has_field?(input_name, type: 'text')
    page.select(value, from: input_name) if page.has_select?(input_name)
  end
end

When(/^я нажимаю "([^"]*)"$/) do |link_or_button|
  page.click_link_or_button link_or_button
end

When(/^система должна предоставить следующие возможности:$/) do |table|
  table.raw.each do |row|
    (page.has_link?(row[0]) || page.has_button?(row[0])).should be_true
  end
end

When(/^система должна показать сообщение "([^"]*)"$/) do |message_text|
  page.should have_text(message_text)
end


