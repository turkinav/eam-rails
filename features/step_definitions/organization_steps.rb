#encoding: utf-8

Пусть(/^есть организация "(.*?)"$/) do |arg1|
  organization = FactoryGirl.create :organization_A
end

When(/^есть следующие организации:$/) do |table|
  table.hashes.each do |row|
    attrs = replace_keys(row, { 'Наименование' => :title })
    p attrs
    Organization.create!(attrs)
  end
end

