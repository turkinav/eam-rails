#encoding: utf-8

Пусть /^в систему внесены следующие задачи:$/ do |table|
  TASK_STATUS_CODES = {"новая" => 0, "выполняется" => 1, "завершена" => 2}
  # table is a Cucumber::Ast::Table
  table.hashes.each do |row|
    Task.create!(title: row['Наименование'], status: TASK_STATUS_CODES[row['Статус']])
  end
end



Пусть /^в систему внесены следующие наряды:$/ do |table|
  ORDER_STATUS_CODES = {"новый" => 0, "выполняется" => 1, "завершен" => 2}
  table.hashes.each do |row|
    WorkOrder.create!(title: row['Наименование'], status: ORDER_STATUS_CODES[row['Статус']])
  end
end

Пусть /^в систему внесены следующие пользователи:$/ do |table|
  table.hashes.each do |row|
    User.create!(name: row['Имя'], button_id: row['Номер карточки'])
  end
end
