# encoding: utf-8

When /^в корзине пользователя "([^"]*)" есть следующие позиции:$/ do |user_name, table|
  user = self.user(user_name)
  user_cart = self.user_cart(user)
  table.hashes.each do |hash|
    inventory = inventory(hash["наименование"])
    user_cart.recieve_inventory(inventory, hash["количество"].to_i)
    user_cart.save()
  end
end
When /^корзина пользователя "([^"]*)" пуста$/ do |user_name|
  user = self.user(user_name)
  user_cart = self.user_cart(user)
  user_cart.should be_empty
end
Then /^в корзине пользователя "([^"]*)" должны быть следующие позиции:$/ do |user_name, table|
  user = self.user(user_name)
  cart = self.user_cart(user)
  cart.items.count.should == table.hashes.count
  table.hashes.each do |row|
    inv = inventory(row["наименование"])
    cart.item_of(inv).value.should == row["количество"].to_f
  end
end

When /^в корзине пользователя "([^"]*)" не должно быть позиций:$/ do |user_name, table|
  user = self.user(user_name)
  cart = self.user_cart(user)

  table.hashes.each do |row|
    inv = inventory(row["наименование"])
    cart.item_of(inv).value.should == 0
  end
end