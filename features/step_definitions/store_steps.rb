# encoding: utf-8

When /^есть склад$/ do
 FactoryGirl.create(:warehouse)
end

When /^остаток по некоторой позиции составляет (\d+)$/ do |amount|
  self.my_store_acc.balance_for(self.my_asset).should == 0
  self.my_store_acc.deposit(self.my_asset, amount.to_i)
end
When /^я сообщаю о приходе данной позиции в количестве (\d+)$/ do |amount|
  self.my_store_acc.deposit(self.my_asset, amount.to_i)
end
Then /^остаток по данной позиции должен стать (\d+)$/ do |amount|
  self.my_store_acc.balance_for(self.my_asset).should == amount.to_i
end
When /^я сообщаю о расходе данной позиции в количестве (\d+)$/ do |amount|
  self.my_store_acc.withdraw(self.my_asset, amount.to_i)
end
Then /^есть некоторая позиция учёта$/ do
  self.my_asset.should_not be_nil
end
Then /^есть некоторый пользователь$/ do
  self.my_user.should_not be_nil
end
When /^остаток по данной позиции на складе составляет (\d+) ед.$/ do |amount|
  self.my_store_acc.balance_for(self.my_asset).should == 0
  self.my_store_acc.deposit(self.my_asset, amount.to_i)
end
When /^количество данной позиции на руках у пользователя составляет (\d+) ед.$/ do |amount|
  self.my_user_acc.balance_for(self.my_asset).should == 0
  self.my_user_acc.deposit(self.my_asset, amount.to_i)
end
When /^этот пользователь ещё ничего не брал на складе$/ do
  step 'количество данной позиции на руках у пользователя составляет 0 ед.'
end
When /^пользователь получает на складе данную позицию в количестве (\d+) ед.$/ do |amount|
  self.transfer(self.my_asset, amount.to_i, self.my_store_acc, self.my_user_acc)
end
Then /^свободный для выдачи остаток по данной позиции на складе должен быть (\d+) ед.$/ do |amount|
  self.my_store_acc.balance_for(self.my_asset).should == amount.to_i
end
When /^количество данной позиции на руках у пользователя должно быть (\d+) ед.$/ do |amount|
  self.my_user_acc.balance_for(self.my_asset).should == amount.to_i
end
When /^пользователь возвращает на склад данную позицию в количестве (\d+) ед.$/ do |amount|
  self.transfer(self.my_asset, amount.to_i, self.my_user_acc, self.my_store_acc)
end
When /^есть следующие остатки на складе:$/ do |table|
  table.hashes.each do |row|
    asset = self.inventory!(row['Наименование'])
    amount = row['Количество'].to_i
    self.my_store_acc.balance_for(asset).should == 0
    self.my_store_acc.deposit(asset, amount)
  end
end
When /^происходит следующее поступление на склад:$/ do |table|
  table.hashes.each do |row|
    asset = self.inventory!(row['Наименование'])
    amount = row['Количество'].to_i
    self.my_store_acc.deposit(asset, amount)
  end
end


When /^на складе "([^\"]+)" есть следующие остатки:$/ do |warehouse_title, table|
  step "на счете ""#{self.warehouse!(warehouse_title).account.title}"" числятся следующие остатки:", table
end

When /^на счете (.+) числятся следующие остатки:$/ do |acc_title, table|
  account = self.account!(acc_title)
  table.hashes.each do |row|
    inventory = self.inventory!(row['наименование'])
    p inventory
    amount = row['количество'].to_f
    p amount
    account.deposit(inventory, amount)
  end
  account.save
end
When /^склад по умолчанию - "(.+)"$/ do |warehouse_title|
  # TODO: set default warehouse for session
  p self.model!(warehouse_title)
end
