# encoding: utf-8

When /^в систему внесены следующие позиции:$/ do |table|
  table.hashes.each do |hash|
    FactoryGirl.create(hash["наименование"].to_sym)
  end
end
