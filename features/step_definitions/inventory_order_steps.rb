# encoding: utf-8

Когда(/^пользователь открывает журнал заказов$/) do
  visit pto_warehouse_inventory_orders_path
end

Тогда(/^система должна показать список заказов$/) do
  page.should have_selector '#inventory-orders'
end

When(/^система должна сохранить заказ в базе данных$/) do
  InventoryOrder.count.should == 1
end

When(/^я создаю новый заказ$/) do
  visit new_pto_warehouse_inventory_order_path
end