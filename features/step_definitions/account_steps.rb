# encoding: utf-8

Пусть /^у пользователя есть следующие позиции:$/ do |table|
  # table is a Cucumber::Ast::Table
  user = User.find_by_login(FactoryGirl.attributes_for(:user)[:login])
  table.hashes.each do |hash|
    item = Inventory.find_by_title(FactoryGirl.attributes_for(hash["наименование"].to_sym)[:title])
    user.account.deposit(item, hash["количество"].to_f)
  end
  user.account.save
end