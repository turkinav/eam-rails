module SkusHelper
  def avail_measuring_units
    MeasuringUnit.order(:title)
  end

  def avail_models
    InventoryModel.order(:title)
  end
end
