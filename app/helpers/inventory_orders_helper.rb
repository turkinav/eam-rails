module InventoryOrdersHelper
  def avail_inventory_articles
    unless @avail_inventory_articles
      search_text = params[:search_text]
      @avail_inventory_articles = (search_text && !search_text.empty?) ?
          InventoryArticle.where(title: search_text) : InventoryArticle.all
    end
    @avail_inventory_articles
  end

  def select_inventories
    params[:recieve_inventory] || params[:search_inventories]
  end
end
