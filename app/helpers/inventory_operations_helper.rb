module InventoryOperationsHelper
  def inventory_operation_type_options
    [ReceiveInventory, WriteOffInventory, TakeInventory, ReleaseInventory].map do |t|
      [t.model_name.human, t.model_name]
    end
  end

  def inventory_operation_type_selected_option(type)
    {selected: [type.model_name.human, type.model_name]} if type
  end
end
