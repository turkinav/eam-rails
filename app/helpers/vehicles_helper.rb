module VehiclesHelper
  def avail_vehicle_models
    VehicleModel.order(:title)
  end
end
