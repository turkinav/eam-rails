module InventoriesHelper
  def highlight? inventory
    inventory.holdings_total_amount > 0
  end

  def highlight_error? inventory
    false
  end

  def highlight_warning? inventory
    false
  end


  def avail_users
    # TODO
    InventoryHolding.join(:user).group(:user_id).order('users.name').map {|h| h.user }
  end
end
