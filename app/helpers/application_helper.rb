module ApplicationHelper

  #def link_to_remove_fields(name, f)
  #  f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  #end
  #
  #def link_to_add_fields(name, f, association)
  #  new_object = f.object.class.reflect_on_association(association).klass.new
  #  fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
  #    render(association.to_s.singularize, :f => builder)
  #  end
  #  link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  #end
  #
  #def current_organization
  #  Organization.first
  #end

  def sortable(column, title = nil)
    column = column.to_s
    title ||= column.titleize
    css_class = (column == sort_column) ? "current #{sort_direction}" : nil
    direction = (column == sort_column && sort_direction == 'asc') ? 'desc' : 'asc'
    link_to title, request.query_parameters.merge({sort:  column, direction: direction}), class: css_class
  end
end
