jQuery ->
  $('#navbar').affix()

  $("[data-link]").click ->
    window.location = $(this).data('link')

  $('.form-horizontal .control-label').each ->
    $(this).text($(this).text() + ':') unless $(this).text().match(/:$/)


  $('.tabs a').click (e) ->
    e.preventDefault()
    $(this).tab('show')


  $('button.mark-for-destruction').click ->
    $(this).prev('input[type=hidden]').val('true')

  $('.modal').modal('show')

  $('.submit-on-change').change ->
    $(this).closest('form').submit()

  # Vehicle picker

  g_vehicles_map = {}

  #
  #  find_vehicles_by = (number) ->
  #    result = []
  #    $.ajax
  #      url: '/vehicles.json?number=' + number
  #      dataType: 'json'
  #      async: false
  #      success: (vehicles, status, request) ->
  #        result = vehicles
  #    result


  build_vehicle_suggestions = (data) ->
    result = []
    for vehicle in data
      do (vehicle) ->
        # map title to id
        g_vehicles_map[vehicle.title] = vehicle.id
        # add to the suggestions list
        result.push(vehicle.title)
    result

  init_vehicle_typeahead = (text_elem, value_elem) ->
    text_elem.attr('autocomplete', 'off')
    text_elem.attr('data-value', text_elem.val())
    text_elem.focusout ->
      add_btn = $(this).siblings('a').first()
      add_btn.attr('href', add_btn.attr('data-url') + '?number=' + $(this).val())
      $(this).val($(this).attr('data-value'))


    # initialize bootstrap typeahead to select from vehicles
    text_elem.typeahead
      source: (query, process) ->
        $.get('/vehicles.json', {number: query, limit: 5}, (data) ->
          process(build_vehicle_suggestions(data)))

      updater: (text) ->
        # setup selected vehicle id
        value_elem.val(g_vehicles_map[text])
        text_elem.attr('data-value', text)
        text


  init_vehicle_typeahead(
    $('.vehicle-picker > .controls > input[type=text]'),
    $('.vehicle-picker > .controls > input[type=hidden]'))

  add_btn = $('.vehicle-picker > .controls > a').first()
  add_btn.attr('data-url', add_btn.attr('href'))

# Vehicle picker ends


  # add table sort direction indicator
  $('.table > thead > tr > th > .current.asc').append("&nbsp;<i class='icon-chevron-up'></i>")
  $('.table > thead > tr > th > .current.desc').append("&nbsp;<i class='icon-chevron-down'></i>")


