# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('.delete-order-item').click ->
    $(this).prev('input[type=hidden]').val('true')
    $(this).closest('.order-item').hide()

  $('.modal').on 'shown', () ->
    $('#inventory_search_text').focus()

  $('.modal').modal('show')




