if barcode.nil?
  json.null!
else
  json.extract! barcode, :type, :value
end

