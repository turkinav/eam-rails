json.sku do
  json.partial! 'sku', sku: inventory_amount.sku
end
json.amount inventory_amount.value
