json.extract! sku, :id
json.title sku.title
json.barcode do
  json.partial! 'barcode', barcode: sku.barcode
end
json.defective :false