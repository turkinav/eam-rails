json.extract! sku, :id, :defective
json.title sku.title
json.barcode do
  json.type sku.barcode.type
  json.value sku.barcode.value
end