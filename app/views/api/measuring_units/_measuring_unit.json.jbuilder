json.extract! measuring_unit, :id, :title
json.url  api_v1_measuring_unit_path(measuring_unit)
json.index_url api_v1_measuring_units_path
