json.extract! inventory_model, :id, :title, :description
json.url  api_v1_inventory_model_path(inventory_model)
json.index_url api_v1_inventory_models_path
