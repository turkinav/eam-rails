json.sku do
  json.partial! 'sku', sku: inventory.sku
end
json.value inventory.value

