json.array! @warehouses do |w|
  json.extract! w, :id, :title, :description
end