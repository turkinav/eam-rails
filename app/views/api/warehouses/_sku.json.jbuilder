json.extract! sku, :id, :defective
json.title sku.title
json.barcode do
  json.type 'ean13'
  json.value sku.barcode.value
end