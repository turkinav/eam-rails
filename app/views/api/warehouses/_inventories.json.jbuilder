json.array! inventories do |inv|
  json.partial! 'inventory', inventory: inv
end