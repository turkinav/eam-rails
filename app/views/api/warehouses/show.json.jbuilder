json.extract! @warehouse, :id, :title, :description
json.url api_v1_warehouse_path(@warehouse)
json.index_url api_v1_warehouses_path
