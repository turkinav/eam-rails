json.extract! vehicle, :id, :number, :section_index
json.title vehicle.title
json.model do
  json.partial! 'vehicle_model', vehicle_model: vehicle.model
end
