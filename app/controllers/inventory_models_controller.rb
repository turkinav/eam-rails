class InventoryModelsController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only: [:show, :edit, :update, :destroy] do
    @inventory_model = InventoryModel.find params[:id]
  end

  before_filter only: [:new, :create] do
    @inventory_model = InventoryModel.new
  end


  # GET /inventory_models
  def index
    @inventory_models = InventoryModel.page(current_page).order(sort_order('title asc'))
  end

  # POST /inventory_models
  def create
    create_or_update
  end


  # PUT /inventory_models/1
  def update
    create_or_update
  end

  # DELETE /inventory_models/1
  def destroy
    @inventory_model.destroy
    flash[:alert] = t('helpers.notices.destroy.success')
    redirect_to inventory_models_url
  end


  def valid_sort_columns
    %w(title)
  end

  private
    def create_or_update
      @inventory_model.attributes = params[:inventory_model]

      if @inventory_model.save
        redirect_to @inventory_model, notice: t('helpers.notices.saved')
      else
        render action: @inventory_model.new_record? ? 'new' : 'edit'
      end
    end
end
