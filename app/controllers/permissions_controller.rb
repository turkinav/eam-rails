class PermissionsController < ApplicationController

  before_filter do
    require_user
  end

  def index
      @permissions = Permission.all

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @permissions }
      end
    end

    # GET /inventories/1
    # GET /inventories/1.json
    def show
      @permission = Permission.find(params[:id])

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @permission }
      end
    end

    # GET /inventories/new
    # GET /inventories/new.json
    def new
      @permission = Permission.new
      @users = User.all
      @groups = Group.all


      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @permission }
      end

    end

    # GET /inventories/1/edit
    def edit
      @permission = Permission.find(params[:id])
      @users = User.all
    end

    # POST /inventories
    # POST /inventories.json
    def create
      @permission = Permission.new(params[:permission])

      respond_to do |format|
        if @permission.save
          format.html { redirect_to @permission, notice: 'Inventory was successfully created.' }
          format.json { render json: @permission, status: :created, location: @permission }
        else
          format.html { render action: "new" }
          format.json { render json: @permission.errors, status: :unprocessable_entity }
        end
      end

    end

    # PUT /inventories/1
    # PUT /inventories/1.json
    def update
      @permission = Permission.find(params[:id])

      respond_to do |format|
        if @permission.update_attributes(params[:permission])
          format.html { redirect_to @permission, notice: 'Inventory was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @permission.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /inventories/1
    # DELETE /inventories/1.json
    def destroy
      @permission = Permission.find(params[:id])
      @permission.destroy

      respond_to do |format|
        format.html { redirect_to permissions_url }
        format.json { head :no_content }
      end
    end

end
