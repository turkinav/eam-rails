class SkusController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only: [:show, :edit, :update, :destroy] do
    @sku = Sku.find params[:id], include: :barcode
  end

  before_filter only: [:new, :create] do
    @sku = Sku.new
  end

  # GET /skus
  def index
    @skus = Sku.page(current_page).joins('LEFT OUTER JOIN barcodes ON barcodes.sku_id = skus.id').joins(:model, :uom).order(sort_order('inventory_models.title asc'))
  end

  # POST /skus
  def create
    create_or_update
  end

  # PUT /skus/1
  def update
   create_or_update
  end

  # DELETE /skus/1
  def destroy
    @sku.destroy
    redirect_to skus_url, alert: t('helpers.notices.destroy.success')
  end

  def valid_sort_columns
    %w(inventory_models.title measuring_units.title barcodes.value)
  end

  private
    def create_or_update
      @sku.attributes = params[:sku]

      if params[:build_barcode]
        @sku.build_barcode if @sku.barcode.blank?
        @sku.barcode.new_value
      end


      if params[:ok] && @sku.save
        redirect_to @sku, notice: t('helpers.notices.saved')
      else
        render action: @sku.new_record? ? 'new' : 'edit'
      end
    end
end
