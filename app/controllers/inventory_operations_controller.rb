require 'filter_by_date_period'

class InventoryOperationsController < ApplicationController

  include ::FilterByDatePeriod

  before_filter do
    require_user
  end

  before_filter only: [:show, :edit, :update, :destroy] do
    find_operation
  end

  before_filter only: [:new, :create] do
    build_operation
    @inventory_operation.warehouse_id = params[:warehouse_id]
  end

  # GET /inventory_operations

  def index
    @inventory_operations = InventoryOperation.within_date_period(period)
      .order(sort_order('registration_time desc, id desc'))
      .page(current_page)
    @inventory_operations = @inventory_operations.where(warehouse_id: warehouse.id) if warehouse
    @inventory_operations = @inventory_operations.where(type: type.model_name) if type
  end

  def create
    create_or_update
  end

  def update
    create_or_update
  end

  def destroy
    @inventory_operation.destroy
    redirect_to inventory_operations_url(session[:index_params]), alert: t('helpers.notices.destroy.success')
  end

  def apply
    begin
      InventoryOperation.find(params[:id]).apply
      flash[:notice] = t('helpers.notices.saved')
    rescue Errors::BalanceError => e
      flash[:error] = e.message
    end
    redirect_to :back
  end


  def valid_sort_columns
    %w(id number type registration_time)
  end

  protected

    def create_or_update
      @inventory_operation.items.reload
      @inventory_operation.attributes = params[@inventory_operation.class.model_name.underscore.to_sym]

      if params[:add_ok] && params[:sku_ids]
        params[:sku_ids].each do |sku_id|
          @inventory_operation.items.add(Sku.find(sku_id), 1)
        end
      end

      if params[:ok] && @inventory_operation.save
        redirect_to @inventory_operation, notice: t('helpers.notices.saved')
      else
        render action: @inventory_operation.new_record? ? 'new' : 'edit'
      end
    end

    def find_operation
      @inventory_operation = InventoryOperation.find params[:id]
    end

    def build_operation
      raise NotImplementedError, 'build_operation'
    end

    def warehouse
      @warehouse ||= params[:warehouse_id].blank? ? Warehouse.first : Warehouse.find_by_id(params[:warehouse_id])
    end

    def type
      unless @type
        @type = (params[:type].camelcase.constantize rescue {}) unless params[:type].blank?
      end
      @type
    end
end
