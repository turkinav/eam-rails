class ApplicationController < ActionController::Base

  protect_from_forgery

  helper_method :current_user_session, :current_user

  def require_user
    unless current_user
      store_location
      flash[:alert] = t 'user_sessions.notices.require_exist'
      redirect_to login_url
      false
    end
  end

  def require_no_user
    if current_user
      store_location
      flash[:alert] = t 'user_sessions.notices.require_not_exist'
      redirect_to account_url
      false
    end
  end

  def store_location
    session[:return_to] = request.url
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  helper_method :sort_column, :sort_direction

  before_filter only: :index do
    session[:index_params] = request.query_parameters
  end


  def sort_column
    @sort_column ||= ensure_valid(params[:sort], valid_sort_columns)
  end

  def valid_sort_columns
    []
  end

  def sort_direction
    @sort_direction ||= ensure_valid(params[:direction], valid_sort_directions) if params[:direction]
  end

  def valid_sort_directions
    %w(desc asc)
  end

  def sort_order(default = '')
    return [sort_column, sort_direction].join(' ') if (sort_column && sort_direction)
    default
  end

  def current_page
    unless @current_page
      @current_page = params[:page].to_i if params[:page]
      @current_page = 1 if (@current_page && @current_page < 1)
    end
    @current_page
  end

  private
    def current_user_session
      @current_user_session ||= UserSession.find
    end

    def current_user
      @current_user ||= current_user_session && current_user_session.user
    end

    def session_key(suffix = '')
      self.class.name.underscore + suffix
    end

    def ensure_valid(param, valid_values, default = nil)
      return param if valid_values.include? param.to_s
      default
    end
end
