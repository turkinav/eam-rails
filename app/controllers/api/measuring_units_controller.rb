module Api
  class MeasuringUnitsController < ApiController
    def index
      @measuring_units = MeasuringUnit.order(:title)
    end

    def show
      @measuring_unit = MeasuringUnit.find params[:id]
    end
  end
end
