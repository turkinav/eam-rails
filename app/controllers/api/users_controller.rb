module Api
  class UsersController < ApiController

    before_filter only: [:show] do
      @user = User.find(params[:id])
    end

    before_filter only: [:index, :export] do
      search_filter = params.select { |key, value| search_params.include?(key.to_sym) }
      @users = search_filter.empty? ? User.order(:name) : User.where(search_filter).order(:name)
    end


    def show;
    end

    # GET /users
    def index;
    end


    def import
      UserUpload.create(request.body)
      render nothing: true, status: :ok
    end


    def export;
    end

    private
      def search_params
        [:button_id]
      end
  end
end

