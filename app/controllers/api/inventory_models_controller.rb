module Api
  class InventoryModelsController < ApiController
    def index
      @inventory_models = InventoryModel.order(:title)
    end

    def show
      @inventory_model = InventoryModel.find params[:id]
    end
  end
end


