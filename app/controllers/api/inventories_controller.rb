class Api::InventoriesController < Api::ApiController

  before_filter only: [:reserve, :release, :available_amounts, :reserved_amounts] do
    @warehouse = Warehouse.find params[:warehouse_id]
  end

  before_filter only: [:reserve, :release, :reserved_amounts] do
    @user = User.find params[:user_id]
  end

  before_filter only: [:reserve, :release] do
    @inventory_amounts = JSON.parse(request.body.read).map { |item| {sku: Sku.find(item['sku_id'].to_i), amount: item['amount'].to_f} }
  end


  def available_amounts
    @inventories = Inventory.joins(sku: [:model]).joins('LEFT JOIN barcodes ON skus.id = barcodes.sku_id').by_warehouse(@warehouse).available
    if params[:search_text]
      search_text = URI.unescape(params[:search_text])
      @inventories = @inventories.where('barcodes.value = ? OR inventory_models.title LIKE ?', search_text, "%#{search_text}%") if search_text
    end
    @inventories = @inventories.order('inventory_models.title')
    @available_amounts = @inventories.map { |inv| InventoryAmount.new(inv.sku, inv.available_amount) }
  end

  def reserved_amounts
    @inventories = Inventory.joins(sku: [:model]).joins('LEFT JOIN barcodes ON skus.id = barcodes.sku_id').by_warehouse(@warehouse).held_by_user(@user)
    if params[:search_text]
      search_text = URI.unescape(params[:search_text])
      @inventories = @inventories.where('barcodes.value = ? OR inventory_models.title LIKE ?', search_text, "%#{search_text}%") if search_text
    end
    @inventories = @inventories.order('inventory_models.title')
    @reserved_amounts = @inventories.map { |inv| InventoryAmount.new(inv.sku, inv.holding_amount(@user)) }
  end


  def reserve
    op = TakeInventory.new(worker: @user, warehouse: @warehouse)
    op.items.add_several @inventory_amounts
    op.save!
    Accounting.apply_operation op
    render nothing: true, status: :ok
  end

  def release
    op = ReleaseInventory.new(worker: @user, warehouse: @warehouse)
    op.items.add_several @inventory_amounts
    op.save!
    Accounting.apply_operation op
    render nothing: true, status: :ok
  end
end



