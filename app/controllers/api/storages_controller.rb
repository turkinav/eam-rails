class Api::StoragesController < Api::ApiController
  def index
    @storages = Storage.order(:order_num)
    @storages = @storages.accessible_by_user_id(params[:user_id]) if params[:user_id]
  end
end