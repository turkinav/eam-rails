class Api::ApiController < ApplicationController
  rescue_from StandardError, with: :handle_exception

  def handle_exception(e)
    render status: :internal_server_error, json: { error: "#{e.inspect}" }
  end
end