module Api
  class WarehousesController < ApiController

    before_filter only: [:show, :available_inventories, :reserved_inventories, :reserve, :release] do
      @warehouse = Warehouse.find params[:id]
    end

    def index
      @warehouses = Warehouse.order :title
    end

    def show; end
  end
end

