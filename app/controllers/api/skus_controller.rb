module Api
  class SkusController < ApiController
    def index
      @skus = Sku.joins(:model, :barcode)

      if params[:search_text]
        search_text = URI.unescape(params[:search_text])
        @skus = @skus.where('barcodes.value = ? OR inventory_models.title LIKE ?', search_text, "%#{search_text}%")
      end

      @skus = @skus.order('inventory_models.title')
    end

    def show
      @sku = Sku.find params[:id]
    end
  end
end