class StoragesController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only: [:show, :edit, :update, :destroy] do
    @storage = Storage.find(params[:id], include: :permissions)
  end

  before_filter only: [:new, :create] do
    @storage = Storage.new
  end

  # GET /storages
  def index
    @storages = Storage.page(current_page).order(sort_order('order_num asc'))
  end


  # POST /storages
  def create
    process_storage_data
  end


  # PUT /storages/1
  def update
    process_storage_data
  end

  # DELETE /storages/1
  def destroy
    @storage.destroy
    redirect_to storages_path, alert: t('storages.destroy.success')
  end


  def valid_sort_columns
    %w(order_num title)
  end

  private

    def process_storage_data
      @storage.attributes = params[:storage]

      build_avail_users if params[:add_users]
      build_avail_groups if params[:add_groups]

      add_permissions if params[:add_ok]

      if params[:commit] && @storage.save
        redirect_to @storage, notice: t('helpers.notices.saved')
      else
        render action: @storage.new_record? ? 'new' :'edit'
      end
    end

    def build_avail_groups
      storage_group_ids = @storage.group_ids
      @groups = storage_group_ids.empty? ? Group.order(:title) : Group.where('id not in (?)', storage_group_ids)
    end

    def build_avail_users
      storage_user_ids = @storage.user_ids
      @users = storage_user_ids.empty? ? User.order(:name) : User.where('id not in (?)', storage_user_ids)
    end

    def add_permissions
      if params[:group_ids]
        params[:group_ids].each do |id|
          @storage.permissions.build(subject: Group.find(id))
        end
      end
      if params[:user_ids]
        params[:user_ids].each do |id|
          @storage.permissions.build(subject: User.find(id))
        end
      end
    end
end

