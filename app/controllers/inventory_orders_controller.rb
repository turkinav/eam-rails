class InventoryOrdersController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only: [:show, :edit, :update, :m11, :receive] do
    @inventory_order = InventoryOrder.find params[:id]
  end

  before_filter only: [:new, :create] do
    @inventory_order = InventoryOrder.new
  end

  # GET /inventory_orders
  def index
    @inventory_orders = InventoryOrder.page(current_page).order(sort_order('created_at desc'))
  end

  # POST /inventory_orders
  def create
    @inventory_order.attributes = params[:inventory_order]

    add_order_items if params[:sku_ids]

    unless (params[:commit] && @inventory_order.save)
      render :new
    else
      redirect_to @inventory_order, notice: t('helpers.notices.new')
    end
  end

  # PUT /inventory_orders/1
  def update
    @inventory_order.items.reload
    @inventory_order.attributes = params[:inventory_order]

    add_order_items if params[:sku_ids]

    unless (params[:commit] && @inventory_order.save)
      render :edit
    else
      redirect_to @inventory_order, notice: t('helpers.notices.saved')
    end
  end

  # DELETE /inventory_orders/1
  def destroy
    redirect_to(:inventory_orders, alert: t('helpers.notices.destroy.success')) if InventoryOrder.destroy(params[:id])
  end

  def m11
    response.headers['Content-Disposition'] = 'attachment; filename=m11.xml'
  end

  def receive
    receive_inventory = ReceiveInventory.new(warehouse: @inventory_order.warehouse)
    @inventory_order.items.each do |order_item|
      receive_inventory.items.add(order_item.sku, order_item.amount)
      receive_inventory.save!
    end
    redirect_to inventory_operation_path(receive_inventory)
  end

  def valid_sort_columns
    %w(number created_at)
  end

  private
    def add_order_items
      Sku.find(params[:sku_ids]).each do |sku|
        create_order_item(sku)
      end
    end

    def create_order_item(sku)
      @inventory_order.items.add(sku, 1)
    end
end

