class UserSessionsController < ApplicationController

  # GET login
  def new
    @user_session = UserSession.new
  end

  def create

    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      redirect_back_or_default(root_path)
    else
      render action: :new
    end
  end

  # GET logout
  def destroy
    current_user_session.destroy
    redirect_to login_path
  end
end
