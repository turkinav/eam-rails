class UserUploadsController < ApplicationController
  def create
    if params[:ok] && params[:upload]
      begin
        UserUpload.create params[:upload].read
        redirect_to users_path, notice: t('helpers.notices.saved')
      rescue => e
        flash[:alert] = e.message.encode!('UTF-8', invalid: :replace, undef: :replace).truncate(1024)
        render 'new'
      end
    else
      redirect_to users_path
    end
  end
end