class UsersController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only: [:new, :create] do
    @user = User.new
  end

  before_filter only: [:show, :edit, :update, :destroy] do
    @user = User.find(params[:id])
  end

  # GET /users
  def index
    @users = User.page(current_page).order(sort_order('name asc'))
  end

  # POST /users
  def create
    create_or_update
  end

  # PUT /users/1
  def update
    create_or_update
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_path, alert: t('helpers.notices.destroy.success')
  end


  def valid_sort_columns
    %w(name login)
  end

  private
    def create_or_update
      @user.group_memberships.reload
      @user.attributes = params[:user]

      find_avail_groups if params[:add_groups]

      if params[:add_ok] && params[:group_ids]
        params[:group_ids].each do |group_id|
          @user.group_memberships.build(group_id: group_id)
        end
      end

      if params[:ok] && @user.save
        redirect_to @user, notice: t('helpers.notices.saved')
      else
        render action: @user.new_record? ? 'new' : 'edit'
      end
    end

    def find_avail_groups
      user_group_ids = @user.group_ids
      @groups = user_group_ids.empty? ? Group.order(:title) : Group.where('id not in (?)', user_group_ids).order(:title)
    end
end
