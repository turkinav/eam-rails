class InventoryReservesController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only: [:show, :edit, :update, :destroy, :create_order] do
    @inventory_reserve = InventoryReserve.find params[:id]
  end

  before_filter only: [:new, :create] do
    @inventory_reserve = InventoryReserve.new
  end

  def index
    @inventory_reserves = InventoryReserve.order(:title)
  end

  def create
    create_or_update
  end

  def update
    create_or_update
  end

  def destroy
    @inventory_reserve.destroy
    redirect_to inventory_reserves_path(session[:index_params]), alert: t('helpers.notices.destroy.success')
  end

  def create_order
    order = InventoryOrder.new(warehouse_id: @inventory_reserve.warehouse_id)
    @inventory_reserve.items.select { |item| item.in_stock_amount < item.amount }.each do |reserve_item|
      order.items.add(reserve_item.sku, reserve_item.amount - reserve_item.in_stock_amount)
    end
    order.save!
    redirect_to inventory_order_path(order)
  end

  protected

  def create_or_update
    @inventory_reserve.items.reload
    @inventory_reserve.attributes = params[:inventory_reserve]

    if params[:add_ok] && params[:sku_ids]
      params[:sku_ids].each do |sku_id|
        @inventory_reserve.items.add(Sku.find(sku_id), 1)
      end
    end

    if params[:commit] && @inventory_reserve.save
      redirect_to @inventory_reserve, notice: t('helpers.notices.saved')
    else
      render action: @inventory_reserve.new_record? ? 'new' : 'edit'
    end
  end
end
