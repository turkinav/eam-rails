class GroupsController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only:[:show, :edit, :update, :destroy] do
    @group = Group.find(params[:id])
  end

  before_filter only:[:new, :create] do
    @group = Group.new
  end

  # GET /groups
  def index
    @groups = Group.order(:title)
  end

  # POST /groups
  def create
    create_or_update
  end

  # PUT /groups/1
  def update
    create_or_update
  end

  # DELETE /groups/1
  def destroy
    @group.destroy
    redirect_to groups_url
  end


  private
   def create_or_update
     @group.memberships.reload
     @group.attributes = params[:group]

     if params[:add_users]
       find_avail_users
     end

     if params[:add_ok] && params[:user_ids]
       params[:user_ids].each do |user_id|
         @group.memberships.build(user_id: user_id)
       end
     end

     if params[:ok] && @group.save
       redirect_to @group, notice: t('helpers.notices.saved')
     else
       render action: @group.new_record? ? 'new' : 'edit'
     end
   end

   def find_avail_users
     group_user_ids = @group.user_ids
     @users = group_user_ids.empty? ? User.order(:name) : User.where('id not in (?)', group_user_ids).order(:name)
   end
end
