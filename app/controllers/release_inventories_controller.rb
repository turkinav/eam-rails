class ReleaseInventoriesController < InventoryOperationsController

  def new
    @inventory_operation.worker = params[:worker_id] ?  User.find(params[:worker_id]) : current_user
    if params[:inventory_id]
      inventory = Inventory.find params[:inventory_id]
      @inventory_operation.warehouse = inventory.warehouse
      @inventory_operation.items.add(inventory.sku, inventory.holding_amount(@inventory_operation.worker))
    end
  end

  protected
    def build_operation
      @inventory_operation = ReleaseInventory.new
    end
end