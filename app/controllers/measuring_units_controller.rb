class MeasuringUnitsController < ApplicationController

  before_filter do
    require_user
  end

  before_filter only: [:show, :edit, :update, :destroy] do
    @measuring_unit = MeasuringUnit.find params[:id]
  end

  before_filter only: [:new, :create] do
    @measuring_unit = MeasuringUnit.new
  end

  def index
    @measuring_units = MeasuringUnit.order :title
  end

  def create
    create_or_update
  end

  def update
    create_or_update()
  end

  def create_or_update
    @measuring_unit.attributes = params[:measuring_unit]
    if @measuring_unit.save
      redirect_to @measuring_unit, notice: t('helpers.notices.saved')
    else
      render action: @measuring_unit.new_record? ? 'new' : 'edit'
    end
  end

  def destroy
    @measuring_unit.destroy
    flash[:alert] = t('helpers.notices.destroy.success')
    redirect_to measuring_units_url
  end

end


