require 'filter_by_date_period'

class ConsumptionReportsController < ApplicationController

  include ::FilterByDatePeriod

  before_filter do
    require_user
    build_report
  end

  def fmu76
    response.headers['Content-Disposition'] = 'attachment; filename=fmu76.xml'
  end

  private
    def warehouse
      @warehouse ||= params[:warehouse_id].blank? ? Warehouse.first : Warehouse.find_by_id(params[:warehouse_id])
    end

    def build_report
      @report = {}
      @report[:period] = period
      @report[:warehouse] = warehouse
      if @report[:warehouse] && @report[:period]
        @report[:items] = []

        Inventory.joins(sku: :model).where(warehouse_id: @report[:warehouse].id).order('inventory_models.title').each do |inventory|

          debit_entries = inventory.warehouse.debit_entries.where(sku_id: inventory.sku_id)
          credit_entries = inventory.warehouse.credit_entries.where(sku_id: inventory.sku_id)

          credit_amount_before = credit_entries.where('valid_time < ?', period_utc.first).sum('amount') || 0
          debit_amount_before = debit_entries.where('valid_time < ?', period_utc.first).sum('amount') || 0
          turnover_amount_before = credit_amount_before - debit_amount_before

          credit_amount = credit_entries.where('valid_time >= ? AND valid_time < ?', period_utc.first, period_utc.last).sum('amount') || 0
          debit_amount = debit_entries.where('valid_time >= ? AND valid_time < ?', period_utc.first, period_utc.last).sum('amount') || 0
          turnover_amount = credit_amount - debit_amount

          end_balance = turnover_amount_before + turnover_amount

          unless (debit_entries.empty? && credit_entries.empty? && end_balance == 0)
            @report[:items] <<
                {
                  sku: inventory.sku,
                  start_balance: turnover_amount_before,
                  credit_amount: credit_amount,
                  debit_amount: debit_amount,
                  end_balance: end_balance
                }
          end
        end
      end
    end
end
