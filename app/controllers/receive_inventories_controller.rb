class ReceiveInventoriesController < InventoryOperationsController

  def index
    super
    @inventory_operations = @inventory_operations.receptions
  end

  protected
    def build_operation
      @inventory_operation = ReceiveInventory.new
    end
end
