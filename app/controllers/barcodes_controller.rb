class BarcodesController < ApplicationController
  before_filter do
    @barcode = Barby::EAN13.new params[:id]
  end

  def draw
    send_data(@barcode.to_svg_with_data, type:'image/svg+xml', disposition: 'inline')
  end
end
