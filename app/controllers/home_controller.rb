class HomeController < ApplicationController
  before_filter do
    require_user
  end
end
