class InventoriesController < ApplicationController

  before_filter do
    require_user
  end

  def index
    @inventories = warehouse.blank? ? [] :
        Inventory.joins(sku: :model).where(warehouse_id: warehouse.id).order('inventory_models.title')
  end

  def show
    @inventory = Inventory.find params[:id]
  end

  private
    def warehouse
      @warehouse ||= params[:warehouse_id].blank? ? Warehouse.first : Warehouse.find_by_id(params[:warehouse_id])
    end
end
