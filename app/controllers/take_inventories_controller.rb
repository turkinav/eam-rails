class TakeInventoriesController < InventoryOperationsController

  def new
    @inventory_operation.worker = current_user
  end

  protected
    def build_operation
      @inventory_operation = TakeInventory.new
    end
end