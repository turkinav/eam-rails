class TakeInventory < InventoryOperation

  attr_accessible :worker_id, :worker
  belongs_to :worker, class_name: 'User', foreign_key: 'worker_id'

  def apply_to_inventory(inventory, amount)
    inventory.give_to_user(self.worker, amount)
  end

  def build_entry(sku, amount)
  end
end
