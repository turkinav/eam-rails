class Barcode < ActiveRecord::Base
  attr_accessible :type, :value, :sku_id, :sku

  belongs_to :sku

  validates_presence_of :type, :value
  validates_uniqueness_of :value, scope: :type

  # Only 1 barcode for same sku is possible
  validates_uniqueness_of :sku_id

  def to_s
    value
  end
end



