class Warehouse < ActiveRecord::Base
  attr_accessible :description, :title
  validates :title, presence: true, uniqueness: true

  has_many :inventories
  has_many :debit_entries, class_name: 'JournalEntry', as: :source_account
  has_many :credit_entries, class_name: 'JournalEntry', as: :dest_account

  def to_s
    self.title
  end


  def inventories_reserved
    result = []
    inventories.each do |inv|
      result << InventoryAmount.new(inv.sku, inv.reserved_amount) if (inv.reserved_amount > 0)
    end
    result
  end


  def inventories_reserved_by user
    result = []
    inventories.each do |inv|
      result << InventoryAmount.new(inv.sku, inv.amount_reserved_by(user)) if (inv.amount_reserved_by(user) > 0)
    end
    result
  end

  def inventories_available
    result = []
    inventories.available.each do |inv|
      result << InventoryAmount.new(inv.sku, inv.available_amount) if (inv.available_amount > 0)
    end
    result
  end
end
