class InventoryOrder < ActiveRecord::Base
  attr_accessible :number, :warehouse_id, :status, :items_attributes

  belongs_to :warehouse
  has_many :items,
           class_name: 'OrderItem',
           foreign_key: :order_id,
           dependent: :destroy,
           extend: AssocExtensions::InventoryItems

  accepts_nested_attributes_for :items, allow_destroy: true

  validates_associated :items
  validates_presence_of :warehouse_id

  after_initialize :setup

  def setup
    if self.new_record?
      build_number
    end
  end

  private
    # TODO: extract to separate module
    def build_number
      last = self.class.order(:id).last
      self.number = '%04i' % (last ? last.id.to_s.last(4).to_i : 0)
    end
end