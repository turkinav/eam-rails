class Ean13 < Barcode

  after_initialize do
    new_value if value.blank?
  end

  validate :value_is_valid

  # TODO ???
  def new_value
    numbers_in_use = Barcode.where('value LIKE ?', DEFAULT_PREFIX + '%').order(:value).pluck(:value).map { |v| v[NUMBER_RANGE].to_i }
    number = get_first_not_in numbers_in_use
    self.value = Ean13.bb(DEFAULT_PREFIX + ('%08d' % number)).data_with_checksum
  end

  def get_first_not_in row
    result = 1
    row.each do |n|
      break if (result != n)
      result = result + 1
    end
    result
  end

  private
    def value_is_valid
      begin
        unless Ean13.bb(value.chop).data_with_checksum == value
          errors.add(:value, 'Checksum is not valid')
        end
      rescue Exception => e
        errors.add(:value, e.message)
      end
    end

    PREFIX_RANGE = 0..3

    def prefix
      value[PREFIX_RANGE]
    end

    NUMBER_RANGE = 4..11

    def number
      value[NUMBER_RANGE]
    end

    DEFAULT_PREFIX = '2100'

    def self.bb data
      Barby::EAN13.new(data)
    end
end
