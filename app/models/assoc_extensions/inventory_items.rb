module AssocExtensions
  module InventoryItems
    def by_sku_id(sku_id)
      live.select {|item| item.sku_id == sku_id }.first || find_by_sku_id(sku_id)
    end

    def for(sku)
      by_sku_id(sku.id)
    end

    def sku_ids
      live.map { |item| item.sku_id }
    end

    def live
      reject(&:marked_for_destruction?)
    end

    def add(sku, amount)
      raise ArgumentError, 'amount' if amount < 0
      result = self.for(sku)
      if result
        result.amount += amount
      else
        result = build(sku: sku, amount: amount) if amount > 0
      end
      result
    end

    def add_several(items = [])
      items.each { |item| add item[:sku], item[:amount] }
    end

    def remove(sku, amount)
      raise ArgumentError, 'amount' if amount < 0
      result = self.for(sku)
      raise ArgumentError, 'sku' unless result
      raise ArgumentError, 'amount' if result.amount < amount
      result.amount -= amount
      result
    end
  end
end
