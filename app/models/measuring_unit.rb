class MeasuringUnit < ActiveRecord::Base
  attr_accessible :title, :description

  validates :title, presence: true, uniqueness: true

  def to_s
    title
  end
end
