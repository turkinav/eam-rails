class GroupMembership < ActiveRecord::Base
  attr_accessible :group_id, :user_id

  belongs_to :user, inverse_of: :group_memberships
  belongs_to :group, inverse_of: :memberships
end
