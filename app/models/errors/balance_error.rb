class Errors::BalanceError < StandardError
  attr_reader :operation

  def initialize(inventory, operation, message = nil)
    @inventory = inventory
    @operation = operation
    @message = message
    @default_message = I18n.t('accounting.balance_error', sku: @inventory.sku, operation: @operation)
  end

  def to_s
    @message || @default_message
  end
end
