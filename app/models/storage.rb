class Storage < ActiveRecord::Base
  attr_accessible :description, :order_num, :title, :warehouse_id, :warehouse, :permissions_attributes

  belongs_to :warehouse

  has_many :permissions, dependent: :destroy
  validates_presence_of :order_num, :warehouse_id
  validates :order_num, numericality: {only_integer: true, greater_than: 0, less_than: 256 }, uniqueness: true

  accepts_nested_attributes_for :permissions, reject_if: lambda { |a| a[:subject_id].blank? }, allow_destroy: true

  before_validation :on_before_validation

  scope(:accessible_by_user_id,
        lambda do |user_id|
          condition = <<EOF
(permissions.subject_type = \'User\' AND permissions.subject_id = ?)
OR EXISTS  (
  SELECT * FROM groups
    INNER JOIN group_memberships ON group_memberships.group_id = groups.id
    WHERE permissions.subject_type = \'Group\' AND permissions.subject_id = groups.id AND group_memberships.user_id = ?)
EOF
          Storage.joins(:permissions).where(condition, user_id, user_id).uniq
        end)

  scope :accessible_to_user, lambda { |user| accessible_by_user_id(user.id) }

  def active_permissions
    permissions.reject(&:marked_for_destruction?)
  end

  def user_ids
    active_permissions.select { |p| p.subject_type == 'User' }.map { |p| p.subject.id }
  end

  def group_ids
    active_permissions.select { |p| p.subject_type == 'Group' }.map { |p| p.subject.id }
  end

  private
    def on_before_validation
      build_title
    end

    def build_title
      (self.title = self.order_num.to_s) if self.title.blank?
    end
end
