class OrderItem < ActiveRecord::Base
  attr_accessible :order_id, :sku_id, :sku, :amount

  belongs_to :order, class_name: 'InventoryOrder'
  belongs_to :sku

  validates_presence_of :sku_id, :amount
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validates_uniqueness_of :sku_id, scope: :order_id

  before_save do
    self.mark_for_destruction unless 0 < self.amount
  end
end
