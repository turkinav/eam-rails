class Inventory < ActiveRecord::Base
  attr_accessible :amount, :holdings_total_amount, :sku, :sku_id, :warehouse, :warehouse_id

  belongs_to :warehouse
  belongs_to :sku

  has_many :holdings, class_name: 'InventoryHolding', dependent: :destroy, autosave: true
  validates_associated :holdings

  scope :by_sku, lambda { |sku| where(sku_id: sku.id) }
  scope :on_balance, where('0 < amount')
  scope :by_warehouse, lambda { |w| where(warehouse_id: w.id) }
  scope :available, where('holdings_total_amount < amount')

  scope :held_by_user, lambda { |user| joins(:holdings).where('inventory_holdings.user_id = ? AND inventory_holdings.amount > 0', user.id) }

  validates_presence_of :sku_id, :warehouse_id
  validates_uniqueness_of :sku_id, scope: :warehouse_id
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validates_numericality_of :holdings_total_amount, greater_than_or_equal_to: 0
  validates_numericality_of :version, greater_than_or_equal_to: 0

  validate do
    if self.amount < self.holdings_total_amount
      errors.add(:amount, I18n.t('accounting.balance_error', inventory: self))
    end
    if self.new_record?
      errors.add(:version, I18n.t('accounting.version_error')) unless self.db_version.nil?
    else
      errors.add(:version, I18n.t('accounting.version_error')) unless self.version == self.db_version
    end
  end

  before_save do
    self.version += 1
  end

  def db_version
    Inventory.where(warehouse_id: warehouse_id, sku_id: sku.id).pluck(:version).first
  end

  def to_s
    "#{self.sku}: #{self.amount}"
  end

  def available_amount
    amount < holdings_total_amount ? 0 : amount - holdings_total_amount
  end

  def give_to_user(user, amount)
    raise ArgumentError, 'amount' if available_amount < amount
    holding(user).amount += amount
    self.holdings_total_amount += amount
  end

  def take_from_user(user, amount)
    raise ArgumentError, 'amount' if holding(user).amount < amount
    holding(user).amount -= amount
    self.holdings_total_amount -= amount
  end

  def holding_amount(user)
    holding(user).amount
  end

  private
    def holding(user)
      # get loaded version OR load from database OR build new
      holdings.select {|h| h.user == user}.first || holdings.by_user(user).first || holdings.build(user: user, amount: 0)
    end
end


