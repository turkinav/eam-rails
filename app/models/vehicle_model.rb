class VehicleModel < ActiveRecord::Base
  attr_accessible :description, :title, :section_count

  SECTION_COUNT_RANGE = 1..4

  validates_presence_of :title, :section_count
  validates_uniqueness_of :title
  validates :section_count, numericality: {only_integer: true}, inclusion: SECTION_COUNT_RANGE


  after_initialize do
    self.section_count ||= 1 if self.new_record?
  end

  def multiple_unit?
    (self.section_count > 1) if self.section_count
  end

  def section_indexes
    (0..self.section_count - 1).map { |i| VehicleSectionIndex.new(i, self.multiple_unit?) } if self.section_count
  end

  def to_s
    self.title
  end
end
