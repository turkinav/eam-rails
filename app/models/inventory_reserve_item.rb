class InventoryReserveItem < ActiveRecord::Base

  attr_accessible :amount, :reserve_id, :sku_id, :sku, :reorder_amount, :reorder_level

  belongs_to :sku
  belongs_to :reserve,
             class_name: 'InventoryReserve',
             foreign_key: :reserve_id,
             inverse_of: :items

  validates_presence_of :amount, :sku_id
  validates_numericality_of :amount, greater_than_or_equal_to: 0
  validates_numericality_of :reorder_level, greater_than_or_equal_to: 0
  validates_numericality_of :reorder_amount, greater_than_or_equal_to: 0

  after_initialize do
    self.amount ||= 0
    self.reorder_amount ||= 0
    self.reorder_level ||= 0
  end

  before_validation do
    self.mark_for_destruction unless (0 < self.amount)
  end

  def to_s
    "#{sku}: #{amount}"
  end

  def in_stock_amount
    inventory = Inventory.find_by_warehouse_id_and_sku_id(reserve.warehouse_id, sku_id)
    @in_stock_amount ||=  inventory ? inventory.amount : 0.0
  end

  def deficit_amount
    result =  self.amount - in_stock_amount
    result = 0 if result < 0
    result
  end

  def warn?
    self.deficit_amount > 0
  end
end
