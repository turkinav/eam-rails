class User < ActiveRecord::Base

  # TODO: class Person name first_name last_name
  # TODO: User.name -> User.person.name

  acts_as_authentic

  attr_accessible :uuid, :name, :number, :button_id, :login, :password, :password_confirmation, :group_memberships_attributes

  has_many :group_memberships, dependent: :destroy
  has_many :groups, through: :group_memberships

  validates_presence_of :name, :login
  validates_uniqueness_of :login
  validates_uniqueness_of :uuid, allow_blank: true, allow_nil: true
  validates_uniqueness_of :button_id, allow_blank: true, allow_nil: true

  after_initialize :on_after_initialize
  before_validation :on_before_validation

  accepts_nested_attributes_for :group_memberships, allow_destroy: true

  def active_group_memberships
    group_memberships.reject(&:marked_for_destruction?)
  end

  def groups
    active_group_memberships.map {|m| m.group }
  end

  def account
    super || create_account(title: name)
  end

  def to_s
    self.title
  end

  def title
    "#{self.name} (#{self.login})"
  end

  private
    def on_after_initialize
      if self.new_record?
        self.uuid = SecureRandom.uuid
        self.login = self.uuid if self.login.blank?
      end
    end

    def on_before_validation
      self.name = self.login if name.blank?
    end
end


