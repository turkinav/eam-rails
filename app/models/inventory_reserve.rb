class InventoryReserve < ActiveRecord::Base
  attr_accessible :title, :description, :items_attributes, :warehouse_id, :warehouse

  belongs_to :warehouse
  has_many :items,
           class_name: 'InventoryReserveItem',
           foreign_key: :reserve_id,
           autosave: true,
           dependent: :destroy,
           extend: AssocExtensions::InventoryItems
  accepts_nested_attributes_for :items, allow_destroy: true

  validates_associated :items
  validates_presence_of :warehouse_id, :title
  validates_uniqueness_of :title, scope: :warehouse_id

  def warn?
    @warn ||= items.any? { |item| item.warn? }
  end
end
