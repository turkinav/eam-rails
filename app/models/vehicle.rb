class Vehicle < ActiveRecord::Base
  # accessible attributes
  attr_accessible :model_id, :model, :number, :section_index

  # associations
  belongs_to :model, class_name: 'VehicleModel', foreign_key: 'model_id'
  has_many :debit_entries, class_name: 'JournalEntry', as: :source_account
  has_many :credit_entries, class_name: 'JournalEntry', as: :dest_account

  # validations
  validates_presence_of :model_id, :number, :section_index
  validates_uniqueness_of :number, scope: [:model_id, :section_index]
  validates :section_index, numericality: {only_integer: true, greater_than_or_equal_to: 0}
  validate :section_index_in_range

  NUMBER_RANGE = 1..99999

  after_initialize do
    self.section_index ||= 0 if new_record?
  end

  def title
    "#{self.model}-#{self.number}#{self.section}"
  end

  def to_s
    self.title
  end

  def section
    VehicleSectionIndex.new(self.section_index, self.model.multiple_unit?) if (self.model && self.section_index)
  end

  private
    def section_index_in_range
      if section_index && model_id
        errors.add(:section_index) unless section_index.between?(0, model.section_count - 1)
      end
    end
end
