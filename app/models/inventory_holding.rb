class InventoryHolding < ActiveRecord::Base
  attr_accessible :amount, :user_id, :user

  belongs_to :inventory, autosave: true
  belongs_to :user

  validates_presence_of :user_id, :inventory_id
  validates_uniqueness_of :inventory_id, scope: :user_id
  validates_numericality_of :amount, greater_than_or_equal_to: 0

  scope :by_user, lambda { |user| where(user_id: user.id) }

  before_validation do
    self.mark_for_destruction unless (0 < self.amount)
  end
end
