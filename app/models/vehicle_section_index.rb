class VehicleSectionIndex
  SECTION_LETTERS = I18n.t('helpers.vehicle_section_letters')

  def initialize(index, multiple_unit)
    @index, @multiple_unit = index, multiple_unit
  end

  def letter
    @multiple_unit ? SECTION_LETTERS[@index] : ''
  end

  attr_reader :index

  def to_s
    self.letter
  end
end

