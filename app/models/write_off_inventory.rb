class WriteOffInventory < InventoryOperation

  attr_accessible :worker_id, :worker, :vehicle_id, :vehicle

  belongs_to :worker, class_name: 'User', foreign_key: 'worker_id'
  belongs_to :vehicle

  def apply_to_inventory(inventory, amount)
    inventory.take_from_user(self.worker, amount) if self.worker
    raise ArgumentError, 'amount' if inventory.amount < amount
    inventory.amount -= amount
  end

  def build_entry(sku, amount)
    JournalEntry.new(
      sku_id: sku.id,
      amount: amount,
      valid_time: self.registration_time,
      source_account: self.warehouse,
      dest_account: self.vehicle) if 0 < amount
  end
end