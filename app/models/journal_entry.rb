class JournalEntry < ActiveRecord::Base

  # accessible attributes
  attr_accessible :amount,
                  :sku_id, :sku,
                  :amount,
                  :valid_time,
                  :source_account_type, :source_account_id, :source_account,
                  :dest_account_type, :dest_account_id, :dest_account

  # associations
  belongs_to :sku
  belongs_to :source_account, polymorphic: true
  belongs_to :dest_account, polymorphic: true

  # validations
  # TODO:ensure valid_time is not in future
  validates_presence_of :amount, :sku_id, :valid_time
  validates_numericality_of :amount, greater_than: 0
end

