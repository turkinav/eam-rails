class Group < ActiveRecord::Base
  attr_accessible :description, :title, :memberships_attributes

  has_many :memberships, class_name:'GroupMembership', dependent: :destroy
  has_many :users, through: :group_memberships

  validates :title, presence: true, uniqueness: true
  accepts_nested_attributes_for :memberships, allow_destroy: true

  def to_s
    self.title
  end

  def active_memberships
    memberships.reject(&:marked_for_destruction?)
  end

  def user_ids
    active_memberships.map {|m| m.user_id }
  end

  def users
    active_memberships.map { |m| m.user }
  end
end
