class Contract < ActiveRecord::Base
  attr_accessible :contractor_id
  belongs_to :organization
  belongs_to :contractor, class_name: "Organization"
end
