class InventoryModel < ActiveRecord::Base
  attr_accessible :description, :title

  has_many :skus, foreign_key: 'model_id'

  validates :title, presence: true, uniqueness: true

  def to_s
    self.title
  end
end
