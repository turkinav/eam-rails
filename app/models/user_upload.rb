# encoding: utf-8

class UserUpload
  def self.create(source)
    users = parse_users(source)
    sql = build_insert_sql(users)
    User.connection.insert sql unless sql.blank?
  end

  private
    def self.parse_users(text)
      result = []
      sep = ';'
      text.each_line do |ln|
        row = ln.strip.split(sep)
        result << User.new(name: row[0], button_id: row[1], password: row[1])
      end
      result
    end

    def self.build_insert_sql(users)
      data = ''
      users.each do |u|
        row_data = "('#{u.uuid}','#{u.name}', '#{u.button_id}', '#{u.uuid}', '#{u.crypted_password}', '#{u.password_salt}', '#{u.persistence_token}', '#{Time.zone.now}', '#{Time.zone.now}')"
        if data.blank?
          data = row_data
        else
          data << ", #{row_data}"
        end
      end
      data.blank? ? '' : 'INSERT INTO users (uuid, name, button_id, login, crypted_password, password_salt, persistence_token, created_at, updated_at) VALUES ' << data
    end
end