class ReceiveInventory < InventoryOperation
  def apply_to_inventory(inventory, amount)
    inventory.amount += amount
  end

  def build_entry(sku, amount)
    JournalEntry.new(
      sku_id: sku.id,
      amount: amount,
      valid_time: self.registration_time,
      dest_account: self.warehouse) if 0 < amount
  end
end
