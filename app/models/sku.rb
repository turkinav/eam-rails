class Sku < ActiveRecord::Base
  attr_accessible :model, :model_id, :uom, :uom_id, :barcode_attributes

  has_one :barcode, class_name: 'Ean13', autosave: true, dependent: :destroy

  validates_associated :barcode
  accepts_nested_attributes_for :barcode

  belongs_to :model, class_name: 'InventoryModel', foreign_key: 'model_id'
  belongs_to :uom, class_name: 'MeasuringUnit', foreign_key: 'uom_id'

  validates_presence_of :model_id, :uom_id
  validates_uniqueness_of :model_id, scope: :uom_id

  before_validation do
    barcode.mark_for_destruction if barcode && barcode.value.blank?
  end

  def title
    "#{self.model.title}, #{self.uom.title}"
  end

  def to_s
    self.title
  end
end
