require File.expand_path('../assoc_extensions/inventory_items', __FILE__)
require File.expand_path('../errors/balance_error', __FILE__)

class InventoryOperation < ActiveRecord::Base
  attr_accessible :type, :number, :warehouse_id, :warehouse, :registration_time, :items_attributes, :description

  belongs_to :warehouse, foreign_key: :warehouse_id

  has_many :items,
           class_name: 'OperationItem',
           autosave: true,
           foreign_key: :operation_id,
           dependent: :destroy,
           extend: AssocExtensions::InventoryItems

  validates_associated :items

  accepts_nested_attributes_for :items, allow_destroy: true

  validate :number_is_valid
  validates_presence_of :type
  validates_presence_of :warehouse_id
  validates_presence_of :registration_time

  scope :posted, where(posted: true)

  scope :within_date_period, lambda { |period| where('registration_time >= ? AND registration_time < ?', period.first.to_date.to_time_in_current_zone.utc, (period.last.to_date + 1.day).to_time_in_current_zone.utc) }

  after_initialize :setup

  def setup
    if self.new_record?
      self.registration_time ||= Time.zone.now
      build_number
    end
  end

  def to_s
    I18n.t('inventory_operations.operation', type: self.class.model_name.human, number: self.number, time: I18n.l(self.registration_time, format: :long))
  end

  def number_is_valid
    if number.present? && registration_time.present?
      scope = InventoryOperation.
          where(number: number).
          where('registration_time >= ?', registration_time.to_date).
          where('registration_time < ?', registration_time.to_date.advance(days: 1))

      scope = scope.where('id != ?', id) if !new_record?
      errors.add(:number) if scope.any?
    end
  end

  def valid_items
    items.reject(&:marked_for_destruction?)
  end

  def posted?
    !self.posted.nil? && self.posted
  end

  def can_apply?
    valid? && !(self.posted? || items.empty?)
  end

  def older_than(other)
    (self.registration_time < other.registration_time)   ||
        (self.registration_time == other.registration_time && self.id < other.id)
  end

  def apply
    raise StandardError unless can_apply?

    operations_to_process = []
    history = InventoryOperation.where(warehouse_id: self.warehouse_id, posted: true).order(:registration_time, :id)

    if (history.empty? || history.last.older_than(self))
      operations_to_process << self
    else
      operations_to_process = history.to_ary
      operation_index = operations_to_process.index { |item| self.older_than(item) }
      operations_to_process.insert(operation_index, self)
    end

    inventories = self.items.map do |item|
      Inventory.find_or_initialize_by_warehouse_id_and_sku_id(self.warehouse_id, item.sku_id)
    end

    entries = []
    inventories.each do |inventory|
      if 1 < operations_to_process.length
        inventory.holdings.clear
        inventory.holdings_total_amount = 0
        inventory.amount = 0
      end

      operations_to_process.each do |operation|
        item = operation.items.by_sku_id(inventory.sku_id)
        if item && (0 < item.amount)
          raise StandardError unless item.valid?
          begin
            operation.apply_to_inventory(inventory, item.amount)
          rescue ArgumentError
            raise Errors::BalanceError.new(inventory, operation)
          end
        end
      end
    end

    # persist changes
    self.transaction do
      inventories.each { |inventory| inventory.save! }
      build_entries.each { |entry| entry.save! }
      self.posted = true
      self.save!
    end
  end

  def apply_to_inventory(inventory, amount)
    raise NotImplementedError, 'apply_to_inventory'
  end

  def build_entries
    result = []
    self.items.live.each do |item|
      entry = build_entry(item.sku, item.amount)
      result << entry if entry
    end
    result
  end

  def build_entry(sku, amount)
    raise NotImplementedError, 'build_entry'
  end

  private
    def build_number
      last = InventoryOperation.order(:id).last
      self.number = '%04i' % (last ? last.id.to_s.last(4).to_i : 0)
    end
end
