class OperationItem < ActiveRecord::Base
  attr_accessible :amount, :sku_id, :sku

  belongs_to :sku
  belongs_to :operation, class_name: 'InventoryOperation'

  validates_presence_of :sku_id, :amount
  validates_numericality_of :amount, greater_than_or_equal_to: 0

  before_validation do
    if self.amount == 0
      if persisted?
        mark_for_destruction
      else
        delete
      end
    end
  end

  def to_s
    "#{self.sku.title}: #{self.amount}"
  end
end
