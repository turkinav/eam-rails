class Permission < ActiveRecord::Base

  attr_accessible :subject, :subject_type, :subject_id
  validates_presence_of :subject

  belongs_to :subject, polymorphic: true
  belongs_to :storage

  def to_s
    self.subject.to_s
  end
end
