class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.integer :warehouse_id
      t.integer :sku_id
      t.decimal :amount,  default: 0
      t.decimal :holdings_total_amount,  default: 0

      t.integer :version, default: 0

      t.timestamps
    end
  end
end
