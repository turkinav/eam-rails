class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :sku_id
      t.decimal :amount

      t.timestamps
    end
  end
end
