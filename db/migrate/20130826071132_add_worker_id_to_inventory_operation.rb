class AddWorkerIdToInventoryOperation < ActiveRecord::Migration
  def change
    add_column :inventory_operations, :worker_id, :integer
  end
end
