class CreateSkus < ActiveRecord::Migration
  def change
    create_table :skus do |t|
      t.integer :model_id
      t.integer :uom_id

      t.timestamps
    end
  end
end
