class AddDescriptionToInventoryOperation < ActiveRecord::Migration
  def change
    add_column :inventory_operations, :description, :text
  end
end
