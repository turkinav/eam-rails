class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.string :title
      t.integer :subject_id
      t.integer :storage_id
      t.string :subject_type

      t.timestamps
    end
  end
end
