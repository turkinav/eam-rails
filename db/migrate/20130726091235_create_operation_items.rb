class CreateOperationItems < ActiveRecord::Migration
  def change
    create_table :operation_items do |t|
      t.integer :operation_id
      t.integer :sku_id
      t.decimal :amount

      t.timestamps
    end
  end
end
