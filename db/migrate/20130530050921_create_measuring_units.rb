class CreateMeasuringUnits < ActiveRecord::Migration
  def change
    create_table :measuring_units do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
