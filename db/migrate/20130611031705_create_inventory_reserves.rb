class CreateInventoryReserves < ActiveRecord::Migration
  def change
    create_table :inventory_reserves do |t|
      t.string :title
      t.text :description
      t.integer :warehouse_id

      t.timestamps
    end
  end
end
