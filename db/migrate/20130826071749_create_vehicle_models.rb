class CreateVehicleModels < ActiveRecord::Migration
  def change
    create_table :vehicle_models do |t|
      t.string :title
      t.integer :section_count
      t.text :description

      t.timestamps
    end
  end
end
