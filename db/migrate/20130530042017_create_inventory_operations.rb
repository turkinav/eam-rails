class CreateInventoryOperations < ActiveRecord::Migration
  def change
    create_table :inventory_operations do |t|
      t.string :type
      t.string :number
      t.integer :warehouse_id
      t.datetime :registration_time
      t.boolean :posted

      t.timestamps
    end
  end
end
