class CreateInventoryReserveItems < ActiveRecord::Migration
  def change
    create_table :inventory_reserve_items do |t|
      t.integer :reserve_id
      t.integer :sku_id
      t.decimal :amount
      t.decimal :reorder_level
      t.decimal :reorder_amount

      t.timestamps
    end
  end
end
