class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.integer :model_id
      t.integer :number
      t.integer :section_index

      t.timestamps
    end
  end
end
