class CreateInventoryHoldings < ActiveRecord::Migration
  def change
    create_table :inventory_holdings do |t|
      t.integer :inventory_id
      t.integer :user_id
      t.decimal :amount

      t.timestamps
    end
  end
end
