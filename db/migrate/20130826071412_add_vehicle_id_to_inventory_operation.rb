class AddVehicleIdToInventoryOperation < ActiveRecord::Migration
  def change
    add_column :inventory_operations, :vehicle_id, :integer
  end
end
