class CreateBarcodes < ActiveRecord::Migration
  def change
    create_table :barcodes do |t|
      t.string :type
      t.string :value
      t.integer :sku_id

      t.timestamps
    end
  end
end
