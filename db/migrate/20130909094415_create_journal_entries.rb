class CreateJournalEntries < ActiveRecord::Migration
  def change
    create_table :journal_entries do |t|
      t.integer :sku_id
      t.decimal :amount
      t.datetime :valid_time

      t.string :source_account_type
      t.integer :source_account_id

      t.string :dest_account_type
      t.integer :dest_account_id

      t.timestamps
    end
  end
end
