class CreateStorages < ActiveRecord::Migration
  def change
    create_table :storages do |t|
      t.integer :order_num
      t.integer :warehouse_id
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
