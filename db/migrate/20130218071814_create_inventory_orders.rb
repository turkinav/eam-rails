class CreateInventoryOrders < ActiveRecord::Migration
  def change
    create_table :inventory_orders do |t|
      t.string :number
      t.integer :warehouse_id
      t.integer :status

      t.timestamps
    end
  end
end
