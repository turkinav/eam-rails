class CreateInventoryModels < ActiveRecord::Migration
  def change
    create_table :inventory_models do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
