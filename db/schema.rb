# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130924024325) do

  create_table "barcodes", :force => true do |t|
    t.string   "type"
    t.string   "value"
    t.integer  "sku_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "group_memberships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "inventories", :force => true do |t|
    t.integer  "warehouse_id"
    t.integer  "sku_id"
    t.decimal  "amount",                :default => 0.0
    t.decimal  "holdings_total_amount", :default => 0.0
    t.integer  "version",               :default => 0
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  create_table "inventory_holdings", :force => true do |t|
    t.integer  "inventory_id"
    t.integer  "user_id"
    t.decimal  "amount"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "inventory_models", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "inventory_operations", :force => true do |t|
    t.string   "type"
    t.string   "number"
    t.integer  "warehouse_id"
    t.datetime "registration_time"
    t.boolean  "posted"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "worker_id"
    t.integer  "vehicle_id"
    t.text     "description"
  end

  create_table "inventory_orders", :force => true do |t|
    t.string   "number"
    t.integer  "warehouse_id"
    t.integer  "status"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "inventory_reserve_items", :force => true do |t|
    t.integer  "reserve_id"
    t.integer  "sku_id"
    t.decimal  "amount"
    t.decimal  "reorder_level"
    t.decimal  "reorder_amount"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "inventory_reserves", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "warehouse_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "journal_entries", :force => true do |t|
    t.integer  "sku_id"
    t.decimal  "amount"
    t.datetime "valid_time"
    t.string   "source_account_type"
    t.integer  "source_account_id"
    t.string   "dest_account_type"
    t.integer  "dest_account_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "measuring_units", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "operation_items", :force => true do |t|
    t.integer  "operation_id"
    t.integer  "sku_id"
    t.decimal  "amount"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "order_items", :force => true do |t|
    t.integer  "order_id"
    t.integer  "sku_id"
    t.decimal  "amount"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "permissions", :force => true do |t|
    t.string   "title"
    t.integer  "subject_id"
    t.integer  "storage_id"
    t.string   "subject_type"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "skus", :force => true do |t|
    t.integer  "model_id"
    t.integer  "uom_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "storages", :force => true do |t|
    t.integer  "order_num"
    t.integer  "warehouse_id"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "uuid",              :null => false
    t.string   "name",              :null => false
    t.string   "number"
    t.string   "button_id"
    t.integer  "group_id"
    t.string   "login",             :null => false
    t.string   "crypted_password",  :null => false
    t.string   "password_salt",     :null => false
    t.string   "persistence_token", :null => false
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "vehicle_models", :force => true do |t|
    t.string   "title"
    t.integer  "section_count"
    t.text     "description"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "vehicles", :force => true do |t|
    t.integer  "model_id"
    t.integer  "number"
    t.integer  "section_index"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "warehouses", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

end
