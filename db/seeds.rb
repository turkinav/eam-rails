# encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users =
  [
  User.create!(name: 'Администратор', login: 'Admin', password: '123456', password_confirmation: '123456')
  ]

warehouses =
  [
  Warehouse.create!(title: 'Склад №1'),
  ]

storages = []
19.times do |i|
  storages << Storage.create!(order_num: i+1, warehouse: warehouses[0], title: "Шкаф №#{i+1}")
  storages[i].permissions.create!(subject: users[0])
end


inventory_models = []
  [
  'Болты с гайками',
  'Вентиль электропневматический контактора ПК-32А ЭТ',
  'Вентиль электропневматический режимного переключателя ПКД-22 ЭТ',
  'Кожуховый болт',
  'Контактор пневматический ПК-32А ЭТ-02 без блокировки',
  'Контактор пневматический ПК-32А ЭТ-02 с блокировкой',
  'Кран концевой 1/4',
  'Кран разобщительный 1/2',
  'Палец блокировочный',
  'Регулировочный болт форсунки',
  'Редуктор крана усл. №130 394.070',
  'Реле дифференциальное РДФ-068',
  'Рукав соединительный',
  'Стабилизатор 259.10.060',
  'Форсунка песочницы',
  'Шайбы пружинные',
  'Шплинты',
  'Шунт ПК',
  'Рукав песочной трубы',
  'Электрощетки ТЛ-2К',
  'Щеткодержатель ТЛ-110',
  'Щеткодержатель НБ-436А',
  'Тяга изоляционная',
  'Контактор МК-15-01',
  'Шунт МК',
  'Реле буксования РБ-4М',
  'Реле рекуперации РР-04',
  'Регулятор давления АК-11б',
  'Контакт угольный неподвижный',
  'Контакт угольный подвижный',
  'Амперметр 75-0-75А',
  'Амперметр 0-75А',
  'Амперметр 750-0-750А',
  'Вольтметр 0-4000v',
  'Манометр МП-2',
  'Кран вспомогательного тормоза усл.№254',
  'Кран трехходовой',
  'Клапан высокого давления',
  'Клапан низкого давления',
  'Редуктор усл. №348',
  'Клапан свистка',
  'Клапан пантографа',
  'Блокировка усл. №367',
  'Лента изоляционная',
  'Крепление форсунок',
  'Шкурка шлифовальная',
  'Щеткодержатель ТЛ-2К',
  'Кран машиниста усл. №394',
  'Форсунки',
  'Шланги',
  'Блок управления',
  'Башмак тормозной',
  'Пружина каретки',
  'Рукав токоприемника',
  'Изолятор опорный',
  'Камера дугогасительная подвижного контакта ПК-41',
  'Камера дугогасительная ПК-22б26',
  'Контактор МКП-23Д',
  'Регулятор напряжения СРН-7У-3',
  'ЗПК',
  'Сочленяющий болт',
  'Валик подвески башмака',
  'Тросики предохранительные',
  'Чека тормозной колодки',
  'Хомут песочной трубы',
  'Крышки заливочных горловин',
  'Шапочные болты',
  'Маятниковые болты',
  'Клин автосцепки СА-3',
  'Крышка нижнего люка ТЛ-2К',
  'Каретка в сборе',
  'Реле усл. №404304',
  'Лакотань',
  'Лента-киперная м',
  'Эмаль изоляционная'
  ].each do |item|
    inventory_models << InventoryModel.create!(title: item)
  end


uoms = []
%w(шт. комплект кг).each do |item|
  uoms << MeasuringUnit.create!(title: item)
end

barcodes =
  [
  {type: 'Ean13', value: '2101000000019'},
  {type: 'Ean13', value: '2101000000026'},
  {type: 'Ean13', value: '2101000000033'},
  {type: 'Ean13', value: '2101000000415'},
  {type: 'Ean13', value: '2101000000040'},
  {type: 'Ean13', value: '2101000000057'},
  {type: 'Ean13', value: '2101000000064'},
  {type: 'Ean13', value: '2101000000071'},
  {type: 'Ean13', value: '2101000000095'},
  {type: 'Ean13', value: '2101000000101'},
  {type: 'Ean13', value: '2101000000118'},
  {type: 'Ean13', value: '2101000000125'},
  {type: 'Ean13', value: '2101000000347'},
  {type: 'Ean13', value: '2101000000354'},
  {type: 'Ean13', value: '2101000000361'},
  {type: 'Ean13', value: '2101000000378'},
  {type: 'Ean13', value: '2101000000385'},
  {type: 'Ean13', value: '2101000000392'},
  {type: 'Ean13', value: '2101000000408'},
  {type: 'Ean13', value: '2101000000422'},
  {type: 'Ean13', value: '2101000000439'},
  {type: 'Ean13', value: '2101000000446'},
  {type: 'Ean13', value: '2101000000149'},
  {type: 'Ean13', value: '2101000000156'},
  {type: 'Ean13', value: '2101000000163'},
  {type: 'Ean13', value: '2101000000170'},
  {type: 'Ean13', value: '2101000000187'},
  {type: 'Ean13', value: '2101000000194'},
  {type: 'Ean13', value: '2101000000200'},
  {type: 'Ean13', value: '2101000000217'},
  {type: 'Ean13', value: '2101000000224'},
  {type: 'Ean13', value: '2101000000231'},
  {type: 'Ean13', value: '2101000000248'},
  {type: 'Ean13', value: '2101000000255'},
  {type: 'Ean13', value: '2101000000262'},
  {type: 'Ean13', value: '2101000000279'},
  {type: 'Ean13', value: '2101000000088'},
  {type: 'Ean13', value: '2101000000286'},
  {type: 'Ean13', value: '2101000000293'},
  {type: 'Ean13', value: '2101000000309'},
  {type: 'Ean13', value: '2101000000316'},
  {type: 'Ean13', value: '2101000000323'},
  {type: 'Ean13', value: '2101000000330'},
  {},
  {type: 'Ean13', value: '2101000000453'},
  {type: 'Ean13', value: '2101000000460'},
  {type: 'Ean13', value: '2101000000477'},
  {type: 'Ean13', value: '2101000000484'},
  {type: 'Ean13', value: '2101000000491'},
  {type: 'Ean13', value: '2101000000507'},
  {type: 'Ean13', value: '2101000000514'},
  {type: 'Ean13', value: '2101000000538'},
  {type: 'Ean13', value: '2101000000545'},
  {type: 'Ean13', value: '2101000000552'},
  {type: 'Ean13', value: '2101000000569'},
  {type: 'Ean13', value: '2101000000576'},
  {type: 'Ean13', value: '2101000000583'},
  {type: 'Ean13', value: '2101000000590'},
  {type: 'Ean13', value: '2101000000606'},
  {type: 'Ean13', value: '2101000000613'},
  {type: 'Ean13', value: '2101000000620'},
  {type: 'Ean13', value: '2101000000637'},
  {type: 'Ean13', value: '2101000000644'},
  {type: 'Ean13', value: '2101000000651'},
  {type: 'Ean13', value: '2101000000668'},
  {type: 'Ean13', value: '2101000000675'},
  {type: 'Ean13', value: '2101000000682'},
  {type: 'Ean13', value: '2101000000699'},
  {type: 'Ean13', value: '2101000000705'},
  {type: 'Ean13', value: '2101000000712'},
  {type: 'Ean13', value: '2101000000729'},
  {type: 'Ean13', value: '2101000000736'},
  {type: 'Ean13', value: '2101000000743'},
  {type: 'Ean13', value: '2101000000750'},
  {type: 'Ean13', value: '2101000000774'}
  ]

skus = []
i = 0
inventory_models.each do |model|
  skus << Sku.create!(model: model, uom: uoms[0], barcode_attributes: barcodes[i])
  i += 1
end

# inventory_operations = []
# 50.times do
#   inventory_operations << ReceiveInventory.create!(warehouse: warehouses[0]) do |op|
#     n_to_skip = rand(0..skus.count())
#     n_to_take = rand(0..(skus.count() - n_to_skip))
#     op.items.add_several(skus.drop(n_to_skip).take(n_to_take).map { |sku| {sku: sku, amount: rand(1..10)} })
#   end
# end

vehicle_models =
  [
  VehicleModel.create!(title: 'ТЭМ2', section_count: 1),
  VehicleModel.create!(title: '2ТЭ10', section_count: 2)
  ]

vehicles = []
[1001, 1002].each do |n|
  vehicles << Vehicle.create!(model: vehicle_models[0], number: n)
end
[2001, 2002].each do |n|
  (0..1).each do |i|
    vehicles << Vehicle.create!(model: vehicle_models[1], number: n, section_index: i)
  end
end
