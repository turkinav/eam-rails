SampleApp::Application.routes.draw do

  resources :consumption_reports, only: [:index] do
    collection do
      get 'fmu76'
    end
  end


  resources :vehicles
  resources :vehicle_models
  resources :warehouses
  resources :skus
  resources :inventory_models
  resources :groups
  resources :users
  resources :storages
  resources :user_uploads, only: [:new, :create]
  resources :inventory_reserves do
    member do
      get 'create_order'
    end
  end

  resources :inventory_operations do
    member do
      post 'apply'
    end
  end

  resources :receive_inventories, except: [:index, :destroy]
  resources :write_off_inventories, except: [:index, :destroy] do
    member do
      get 'vehicle_picker'
    end
  end

  resources :take_inventories, except: [:index, :destroy]
  resources :release_inventories, except: [:index, :destroy]


  resource :user_sessions, only: [:create]
  resources :users

  resources :inventories

  resources :measures
  resources :measuring_units
  resources :organizations
  resources :inventory_orders do
    member do
      get 'm11'
      get 'receive'
    end
  end
  resources :barcodes, only: [:show] do
    member do
      get 'draw'
      get 'print'
    end
  end
  
  match 'login' => 'user_sessions#new'
  match 'logout' => 'user_sessions#destroy'

  namespace :api, defaults: {format: 'json'} do

    resources :users, only: [:index, :show] do
      collection do
        post 'import'
      end
    end

    resources :inventories do
      collection do
        get 'available_amounts'
        get 'reserved_amounts'
        post 'reserve'
        post 'release'
      end
    end


    resources :inventory_models, only: [:index, :show]
    resources :measuring_units, only: [:index, :show]
    resources :skus, only: [:index, :show]
    resources :storages, only: [:index]
  end


  # match 'users/authenticate/:button_id' => 'task_tracker::task_tracker_api#authenticate'
  # match 'tasks(/:user_id(/:status))' => 'task_tracker::task_tracker_api#tasks'
  # match 'users/authenticate/:button_id' => 'task_tracker::task_tracker_api#authenticate'

  root to: "inventory_operations#index"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
